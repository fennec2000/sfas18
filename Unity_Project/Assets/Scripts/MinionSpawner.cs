﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionSpawner : MonoBehaviour
{
    public enum MinionState { Walking, Attacking };

    public Vector3[] Team1Waypath;
    public Vector3[] Team2Waypath;
    public GameObject[] TeamDoors;
    public GameObject minionPrefab;
    public int timeBetweenWaves;
    public int waveSize;
    public float spawnRate;
    public Vector3[] spawnPos;
    public Vector3[] spawnRot;
    public GameObject[] warpers;
    public Material[] minionTeamColours;
    public float minionUpgradeTime; // time in min when minion upgrades

    private float countdown;
    private bool spawning;
    private int leftToSpawn;
    private float nextSpawn;
    private Quaternion[] spawnQuat;
    private List<GameObject> Team1Minions;
    private List<GameObject> Team2Minions;

    // Use this for initialization
    void Start ()
    {
        // var init
        countdown = timeBetweenWaves;
        spawning = true;
        leftToSpawn = waveSize;
        nextSpawn = 0.0f;

        Team1Minions = new List<GameObject>();
        Team2Minions = new List<GameObject>();

        spawnQuat = new Quaternion[spawnRot.Length];

        for (int i = 0; i < spawnRot.Length; ++i)
        {
            spawnQuat[i] = Quaternion.Euler(spawnRot[i]);
        }

        // checks
        Debug.Assert(waveSize * spawnRate < timeBetweenWaves, "Spawning takes longer than wave time.");
	}
	
	// Update is called once per frame
	void Update ()
    {
        switch(this.GetComponent<StartingScript>().theGameState)
        {
            case StartingScript.GameState.Playing:
                // spawn timer
                countdown -= Time.deltaTime;

                if (0 >= countdown) 
                {
                    spawning = true;
                    countdown = timeBetweenWaves;
                }

                if (spawning)
                {
                    // spread out the spawns not all at once
                    nextSpawn -= Time.deltaTime;

                    if (0.0f >= nextSpawn)
                    {
                        for (int t = 0; t < (int)StartingScript.Team.NumOfTeams; ++t)
                        {
                            SpawnMinion((StartingScript.Team)t);
                        }

                        nextSpawn = spawnRate;
                        --leftToSpawn;
                    }

                    if (0 >= leftToSpawn)
                    {
                        nextSpawn = 0.0f;
                        leftToSpawn = waveSize;
                        spawning = false;
                    }
                }
                break;
        }
    }

    void SpawnMinion(StartingScript.Team teamToSpawn)
    {
        // sound
        foreach (GameObject warp in warpers)
            warp.GetComponent<WarperScript>().WarpSound();
         
        // spawn minion
        GameObject newMinion;
        newMinion = Instantiate(minionPrefab, spawnPos[(int)teamToSpawn], spawnQuat[(int)teamToSpawn]);
        newMinion.GetComponent<MinionController>().SetMinionTeam(teamToSpawn); // set team
        newMinion.GetComponent<MinionController>().SetGameManager(gameObject); // give game manager
        newMinion.GetComponent<MinionController>().SetUpgrade((int)(GetComponent<StartingScript>().inGameMin / minionUpgradeTime)); // set the minions strenght
        newMinion.transform.GetChild(0).GetComponent<MeshRenderer>().material = minionTeamColours[(int)teamToSpawn];
        newMinion.name = "MinionCloneT" + (int)teamToSpawn; // name
        newMinion.transform.SetParent(GameObject.Find("MinionsHolder").transform); // organise in unity for viewer

        if (StartingScript.Team.Team1 == teamToSpawn) // add to minion team list
            Team1Minions.Add(newMinion);
        else
            Team2Minions.Add(newMinion);
    }

    // get minion list
    public List<GameObject> GetMinionList(StartingScript.Team team)
    {
        if (StartingScript.Team.Team1 == team)
            return Team1Minions;
        else
            return Team2Minions;
    }

    // remove minion from list - when it dies
    public void RemoveMinion(GameObject theMinion)
    {
        if (theMinion.GetComponent<MinionController>().myTeam == StartingScript.Team.Team1)
        {
            Team1Minions.Remove(theMinion);
            Team1Minions.Sort();
        }
        else
        {
            Team2Minions.Remove(theMinion);
        }
    }
}
