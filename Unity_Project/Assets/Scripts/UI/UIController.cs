﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class UIController : MonoBehaviour
{
    private enum UI_Menus { Base, Power, Tower, Minion, NumOfUI_Menu };
    private enum UI_Powers { None, Base, Unit, Commands, Powers, Select, Attack, Defend, SpecialPower, Repair, Upgrade, NumOfUI_Powers };
    private enum UI_PowerGameObject { Base, Top, Left, Right, Bottom, Text, NumOfUI_PowerGameObject };
    private enum UI_ButtonState { Enabled, Disabled };

    public GameObject GameManger;
    public GameObject[] UI_BaseOptions;
    public GameObject[] UI_PowerOptions;
    public GameObject[] UI_TowerOptions;
    public GameObject[] UI_MinionOptions;
    public GameObject CameraObj;
    public GameObject thePlayer;
    public StartingScript.Team myTeam;
    public StartingScript.Team myEnemy;
    public GameObject[] powersUI;
    public GameObject selectorPrefab;
    public Vector3 selectorDefaultPos;
    public GameObject theGameManager;
    public float selectorDistance;
    public GameObject PowerPrefab;
    public int currentItem;
    public float OverlordSpeed;
    public Vector4 mapSize;

    private int currentMenu;
    private bool buttonSelected;
    private Quaternion CameraQuaternion;
    private float vertInput;
    private float horInput;
    private bool fireDown;
    private Color UISelected;
    private Color UIUnselected;
    private Color UINoAccess;
    private bool inPowersMenu;
    private UI_Powers currentUIPowersMenu;
    public GameObject currentlySelectedUnit;
    private Vector3 currentLocation;
    private Vector3 hiddenPos;
    private GameObject mySelector;
    private bool selectorHidden;
    private Color[] UIColours;


    // Use this for initialization
    void Awake()
    {
        // setup
        theGameManager = GameObject.Find("GameManager");
        hiddenPos = new Vector3(0, -1000, 0);
        mySelector = Instantiate(selectorPrefab, hiddenPos, Quaternion.Euler(0, 0, 0));
        mySelector.name = "Selector" + (int)myTeam;
        selectorHidden = true;

        UISelected = new Vector4(0.6f, 0.6f, 1.0f, 1.0f);
        UIUnselected = new Vector4(1, 1, 1, 1);
        UINoAccess = new Vector4(1, 0.3f, 0.3f, 1);

        UI_BaseOptions[currentItem].GetComponent<Image>().color = UISelected;
        UI_BaseOptions[(int)UI_Menus.Minion - 1].GetComponent<Image>().color = UINoAccess;

        inPowersMenu = false;
        currentUIPowersMenu = UI_Powers.None;

        switch (myTeam)
        {
            case StartingScript.Team.Team1:
                myEnemy = StartingScript.Team.Team2;
                break;
            case StartingScript.Team.Team2:
                myEnemy = StartingScript.Team.Team1;
                break;
        }
    }

    private void Start()
    {
        // setup
        UIColours = new Color[] { new Color(1, 1, 1, 1), new Color(1, 0.3f, 0.3f, 1) };
    }

    // Update is called once per frame
    void Update()
    {
        if (UI_BaseOptions[(int)UI_Menus.Minion - 1].GetComponent<Image>().color == UINoAccess &&
            theGameManager.GetComponent<StartingScript>().theGameState == StartingScript.GameState.Playing)
            UI_BaseOptions[(int)UI_Menus.Minion - 1].GetComponent<Image>().color = UIUnselected;

        // controlls
        switch (myTeam)
        {
            case StartingScript.Team.Team1:
                horInput = Input.GetAxisRaw("Horizontal_P1");
                vertInput = Input.GetAxisRaw("Vertical_P1");
                fireDown = Input.GetButtonDown("Fire_P1");
                break;
            case StartingScript.Team.Team2:
                horInput = Input.GetAxisRaw("Horizontal_P2");
                vertInput = Input.GetAxisRaw("Vertical_P2");
                fireDown = Input.GetButtonDown("Fire_P2");
                break;
            default:
                horInput = 0.0f;
                vertInput = 0.0f;
                fireDown = false;
                Debug.Log("Unknown input detected.");
                break;
        }

        // picking state
        if (PlayerScript.PlayerState.Comander == thePlayer.GetComponent<PlayerScript>().currentState)
        {
            // input
            if (horInput != 0 && !buttonSelected)
            {
                switch (currentMenu) // hide last button colour
                {
                    case (int)UI_Menus.Base:
                        UI_BaseOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                        break;
                    case (int)UI_Menus.Power:
                        UI_PowerOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                        break;
                    case (int)UI_Menus.Tower:
                        UI_TowerOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                        break;
                    case (int)UI_Menus.Minion:
                        UI_MinionOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                        break;
                }

                if (horInput > 0) // next
                {
                    ++currentItem;
                    if ((int)UI_Menus.Base == currentMenu && UI_BaseOptions.Length <= currentItem ||
                       (int)UI_Menus.Power == currentMenu && UI_PowerOptions.Length <= currentItem ||
                       (int)UI_Menus.Tower == currentMenu && UI_TowerOptions.Length <= currentItem ||
                       (int)UI_Menus.Minion == currentMenu && UI_MinionOptions.Length <= currentItem ||
                       (int)UI_Menus.Base == currentMenu && currentItem == (int)UI_Menus.Minion - 1 &&
                       theGameManager.GetComponent<StartingScript>().theGameState != StartingScript.GameState.Playing)
                    {
                        currentItem = 0;
                    }
                }
                else // back
                {
                    --currentItem;
                    if (0 > currentItem)
                    {
                        switch (currentMenu)
                        {
                            case (int)UI_Menus.Base:
                                currentItem = UI_BaseOptions.Length - 1;
                                if (theGameManager.GetComponent<StartingScript>().theGameState != StartingScript.GameState.Playing)
                                    --currentItem;
                                break;
                            case (int)UI_Menus.Power:
                                currentItem = UI_PowerOptions.Length - 1;
                                break;
                            case (int)UI_Menus.Tower:
                                currentItem = UI_TowerOptions.Length - 1;
                                break;
                            case (int)UI_Menus.Minion:
                                currentItem = UI_MinionOptions.Length - 1;
                                break;
                        }
                    }
                }

                switch (currentMenu) // set current button colour
                {
                    case (int)UI_Menus.Base:
                        UI_BaseOptions[currentItem].GetComponent<Image>().color = UISelected;
                        break;
                    case (int)UI_Menus.Power:
                        UI_PowerOptions[currentItem].GetComponent<Image>().color = UISelected;
                        break;
                    case (int)UI_Menus.Tower:
                        UI_TowerOptions[currentItem].GetComponent<Image>().color = UISelected;
                        break;
                    case (int)UI_Menus.Minion:
                        UI_MinionOptions[currentItem].GetComponent<Image>().color = UISelected;
                        break;
                }

                buttonSelected = true;
            }

            if ((vertInput != 0 || fireDown) && !buttonSelected)
            {
                // confirm
                if (vertInput > 0 || fireDown)
                {
                    if ((int)UI_Menus.Base == currentMenu)
                    {
                        switch (currentMenu) // hide last button colour
                        {
                            case (int)UI_Menus.Base:
                                UI_BaseOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                                break;
                            case (int)UI_Menus.Power:
                                UI_PowerOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                                break;
                            case (int)UI_Menus.Tower:
                                UI_TowerOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                                break;
                            case (int)UI_Menus.Minion:
                                if (theGameManager.GetComponent<StartingScript>().theGameState != StartingScript.GameState.Playing)
                                    UI_MinionOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                                break;
                        }

                        if (currentItem == (int)UI_Menus.Power - 1) // go to powers mode
                        {
                            thePlayer.GetComponent<PlayerScript>().SetState(PlayerScript.PlayerState.Power);
                        }
                        else
                        {
                            currentMenu = currentItem + 1; // next set of menus

                            SetMenuActive(true);

                            switch (currentMenu)
                            {
                                case (int)UI_Menus.Base:
                                    UI_BaseOptions[currentItem].GetComponent<Image>().color = UISelected;
                                    break;
                                case (int)UI_Menus.Power:
                                    UI_PowerOptions[currentItem].GetComponent<Image>().color = UISelected;
                                    break;
                                case (int)UI_Menus.Tower:
                                    UI_TowerOptions[currentItem].GetComponent<Image>().color = UISelected;
                                    break;
                                case (int)UI_Menus.Minion:
                                    UI_MinionOptions[currentItem].GetComponent<Image>().color = UISelected;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        switch (currentMenu) // actions
                        {
                            case (int)UI_Menus.Power:
                                thePlayer.GetComponent<PlayerScript>().SetState(PlayerScript.PlayerState.Power);
                                break;
                            case (int)UI_Menus.Tower:
                                UI_TowerOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                                thePlayer.GetComponent<PlayerScript>().SetState(PlayerScript.PlayerState.Tower);
                                break;
                            case (int)UI_Menus.Minion:
                                thePlayer.GetComponent<ChampManager>().SpawnChamp((ChampController.ChampType)currentItem);
                                break;
                            default:
                                break;
                        }
                    }

                    buttonSelected = true;
                }
                //back
                else if ((int)UI_Menus.Base != currentMenu)
                {
                    switch (currentMenu)
                    {
                        case (int)UI_Menus.Base:
                            UI_BaseOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                            break;
                        case (int)UI_Menus.Power:
                            UI_PowerOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                            break;
                        case (int)UI_Menus.Tower:
                            UI_TowerOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                            break;
                        case (int)UI_Menus.Minion:
                            UI_MinionOptions[currentItem].GetComponent<Image>().color = UIUnselected;
                            break;
                    }

                    SetMenuActive(false);
                    currentMenu = (int)UI_Menus.Base;
                    UI_BaseOptions[currentItem].GetComponent<Image>().color = UISelected;
                }

                buttonSelected = true;
            }
        }
        else if (PlayerScript.PlayerState.Power == thePlayer.GetComponent<PlayerScript>().currentState)
        {
            // let the player move the move the selector
            if (currentUIPowersMenu == UI_Powers.None || currentUIPowersMenu == UI_Powers.Attack || currentUIPowersMenu == UI_Powers.Defend ||
                currentUIPowersMenu == UI_Powers.Repair || currentUIPowersMenu == UI_Powers.SpecialPower || currentUIPowersMenu == UI_Powers.Upgrade) // menus to move the selectoro on
            {
                if (myTeam == StartingScript.Team.Team1)
                {
                    Vector3 newPos = new Vector3(-vertInput, 0, horInput) * Time.deltaTime * OverlordSpeed;
                    if ((mySelector.transform.position + newPos).x < mapSize[0] && (mySelector.transform.position + newPos).x > mapSize[2] &&
                        (mySelector.transform.position + newPos).z < mapSize[1] && (mySelector.transform.position + newPos).z > mapSize[3])
                    mySelector.transform.position += new Vector3(-vertInput, 0, horInput) * Time.deltaTime * OverlordSpeed;
                }
                else
                {
                    Vector3 newPos = new Vector3(vertInput, 0, -horInput) * Time.deltaTime * OverlordSpeed;
                    if ((mySelector.transform.position + newPos).x < mapSize[0] && (mySelector.transform.position + newPos).x > mapSize[2] &&
                        (mySelector.transform.position + newPos).z < mapSize[1] && (mySelector.transform.position + newPos).z > mapSize[3])
                        mySelector.transform.position += newPos;
                }
            }
                
            if (currentUIPowersMenu == UI_Powers.None && currentlySelectedUnit != null)
                mySelector.transform.position = currentlySelectedUnit.transform.position; // focus on selected unit

            if (fireDown && !buttonSelected)
            {
                switch (currentUIPowersMenu)
                {
                    case UI_Powers.None: // open menu
                        {
                            MenuSwap(UI_Powers.Base);
                            currentlySelectedUnit = theGameManager.GetComponent<StartingScript>().GetClosestGameObject(gameObject, myTeam, selectorDistance, false, true, true, false);
                        }
                        break;
                    case UI_Powers.Attack:
                        GameObject attackTarget = theGameManager.GetComponent<StartingScript>().GetClosestGameObject(mySelector, myEnemy, selectorDistance);
                        if (currentlySelectedUnit != null)
                        {
                            Debug.Log(currentlySelectedUnit.ToString());
                            if (currentlySelectedUnit.GetComponent<ChampController>() != null) // if champ
                            {
                                if (attackTarget != null)
                                    currentlySelectedUnit.GetComponent<ChampController>().AttackUnit(attackTarget);
                                else
                                    currentlySelectedUnit.GetComponent<ChampController>().ForceMode(ChampController.ChampMode.Attack, mySelector.transform.position);
                            }
                            else if (currentlySelectedUnit.GetComponent<TowerController>() != null) // if tower
                            {
                                if (attackTarget != null)
                                    currentlySelectedUnit.GetComponent<TowerController>().forcedTarget = attackTarget;
                            }
                        }
                        MenuSwap(UI_Powers.Commands);
                        break;
                    case UI_Powers.Defend:
                        if (currentlySelectedUnit != null && currentlySelectedUnit.GetComponent<ChampController>() != null) // if champ
                        {
                            currentlySelectedUnit.GetComponent<ChampController>().ForceMode(ChampController.ChampMode.Hold, mySelector.transform.position);
                        }
                        MenuSwap(UI_Powers.Commands);
                        break;
                    case UI_Powers.SpecialPower:
                        GameObject power = Instantiate(PowerPrefab);
                        power.GetComponent<EMPBombScript>().Setup(theGameManager, myEnemy, mySelector.transform.position);
                        power.name = "EMPBomb";
                        MenuSwap(UI_Powers.Powers);
                        break;
                    case UI_Powers.Repair:
                        if (currentlySelectedUnit != null && currentlySelectedUnit.GetComponent<TowerController>() != null) // if tower
                        {
                            currentlySelectedUnit.GetComponent<TowerController>().ToggleRepair();
                        }
                        MenuSwap(UI_Powers.Powers);
                        break;
                    case UI_Powers.Upgrade:
                        GameObject selecetedTarget = theGameManager.GetComponent<StartingScript>().GetClosestGameObject(mySelector, myTeam, selectorDistance, false);
                        if (selecetedTarget != null)
                        {
                            if (selecetedTarget.GetComponent<ChampController>() != null && selecetedTarget.GetComponent<ChampController>().AffordUpgrade())
                            {
                                selecetedTarget.GetComponent<ChampController>().UpgradeUnit();
                            }
                            else if (selecetedTarget.GetComponent<TowerController>() != null && selecetedTarget.GetComponent<TowerController>().AffordUpgrade())
                            {
                                selecetedTarget.GetComponent<TowerController>().UpgradeUnit();
                            }
                        }
                        MenuSwap(UI_Powers.Powers);
                        break;
                    default: // close menu
                        MenuSwap(UI_Powers.None);
                        break;
                }

                buttonSelected = true;
            }

            if (horInput != 0 && !buttonSelected && inPowersMenu)
            {
                if (horInput > 0)
                {
                    switch (currentUIPowersMenu)
                    {
                        case UI_Powers.Base: // powers
                            MenuSwap(UI_Powers.Powers);
                            break;
                        case UI_Powers.Unit: // priority
                            if (currentlySelectedUnit != null && currentlySelectedUnit.GetComponent<ChampController>() != null) // if champ
                            {
                                switch (currentlySelectedUnit.GetComponent<ChampController>().myStyle)
                                {
                                    case ChampController.ChampStyle.Aggressive:
                                        currentlySelectedUnit.GetComponent<ChampController>().myStyle = ChampController.ChampStyle.Defensive;
                                        break;
                                    case ChampController.ChampStyle.Defensive:
                                        currentlySelectedUnit.GetComponent<ChampController>().myStyle = ChampController.ChampStyle.Aggressive;
                                        break;
                                    default:
                                        break;
                                }
                            }
                            MenuSwap(UI_Powers.Unit);
                            break;
                        case UI_Powers.Commands: // auto
                            if (currentlySelectedUnit != null && currentlySelectedUnit.GetComponent<ChampController>() != null) // if champ
                            {
                                currentlySelectedUnit.GetComponent<ChampController>().ChangeMode(ChampController.ChampMode.Auto);
                            }
                            MenuSwap(UI_Powers.Unit);
                            break;
                        case UI_Powers.Powers: // upgrade
                            if (currentlySelectedUnit != null)
                            {
                                if (currentlySelectedUnit.GetComponent<TowerController>() != null && currentlySelectedUnit.GetComponent<TowerController>().AffordUpgrade()) // if tower
                                {
                                    currentlySelectedUnit.GetComponent<TowerController>().UpgradeUnit();
                                }
                                else if (currentlySelectedUnit.GetComponent<ChampController>() != null && currentlySelectedUnit.GetComponent<ChampController>().AffordUpgrade())
                                {
                                    currentlySelectedUnit.GetComponent<ChampController>().UpgradeUnit();
                                }
                            }
                            MenuSwap(UI_Powers.Powers);
                            break;
                        case UI_Powers.Select: // cycle towers
                            currentlySelectedUnit = thePlayer.GetComponent<BuildManager>().GetNextTower(currentlySelectedUnit);
                            if (currentlySelectedUnit != null)
                                mySelector.transform.position = currentlySelectedUnit.transform.position;
                            MenuSwap(UI_Powers.Select);
                            break;
                        default:
                            break;
                    }
                }
                else if (horInput < 0)
                {
                    switch (currentUIPowersMenu)
                    {
                        case UI_Powers.Base: // unit
                            MenuSwap(UI_Powers.Unit);
                            break;
                        case UI_Powers.Unit: // select
                            currentLocation = transform.position;
                            MenuSwap(UI_Powers.Select);
                            break;
                        case UI_Powers.Commands: // hold
                            MenuSwap(UI_Powers.Defend);
                            break;
                        case UI_Powers.Powers: // repair
                            MenuSwap(UI_Powers.Repair);
                            break;
                        case UI_Powers.Select: // cycle champs
                            currentlySelectedUnit = thePlayer.GetComponent<ChampManager>().GetNextChamp(currentlySelectedUnit);
                            if (currentlySelectedUnit != null)
                                mySelector.transform.position = currentlySelectedUnit.transform.position;
                            MenuSwap(UI_Powers.Select);
                            break;
                        default:
                            break;
                    }
                }

                buttonSelected = true;
            }

            if (vertInput != 0 && !buttonSelected && inPowersMenu)
            {
                if (vertInput > 0)
                {
                    switch (currentUIPowersMenu)
                    {
                        case UI_Powers.Base: // change camera
                            CameraObj.GetComponent<CameraController>().ChangeCameraMode();
                            break;
                        case UI_Powers.Unit: // commands
                            if (currentlySelectedUnit != null)
                                MenuSwap(UI_Powers.Commands);
                            break;
                        case UI_Powers.Commands: // attack
                            MenuSwap(UI_Powers.Attack);
                            break;
                        case UI_Powers.Powers: // EMP
                            MenuSwap(UI_Powers.SpecialPower);
                            break;
                        default:
                            break;
                    }
                }
                else if (vertInput < 0)
                {
                    // Back
                    switch (currentUIPowersMenu)
                    {
                        case UI_Powers.Base: // back to commander mode
                            MenuSwap(UI_Powers.None);
                            thePlayer.GetComponent<PlayerScript>().SetState(PlayerScript.PlayerState.Comander);
                            break;
                        case UI_Powers.Unit: // back to base
                            MenuSwap(UI_Powers.Base);
                            break;
                        case UI_Powers.Commands: // back to unit
                            MenuSwap(UI_Powers.Unit);
                            break;
                        case UI_Powers.Powers: // back to base
                            MenuSwap(UI_Powers.Base);
                            break;
                        case UI_Powers.Select: // back to unit
                            transform.position = currentLocation;
                            MenuSwap(UI_Powers.Unit);
                            break;
                        default:
                            break;
                    }
                }
            }

            buttonSelected = true;
        }

        if (horInput == 0 && vertInput == 0)
        {
            buttonSelected = false;
        }
    }

    void SetMenuActive(bool toEnable)
    {
        switch (currentMenu)
        {
            case (int)UI_Menus.Base:
                for (int i = 0; i < UI_BaseOptions.Length; ++i)
                {
                    UI_BaseOptions[i].SetActive(toEnable);
                }
                break;
            case (int)UI_Menus.Power:
                for (int i = 0; i < UI_PowerOptions.Length; ++i)
                {
                    UI_PowerOptions[i].SetActive(toEnable);
                }
                break;
            case (int)UI_Menus.Tower:
                for (int i = 0; i < UI_TowerOptions.Length; ++i)
                {
                    UI_TowerOptions[i].SetActive(toEnable);
                }
                break;
            case (int)UI_Menus.Minion:
                for (int i = 0; i < UI_MinionOptions.Length; ++i)
                {
                    UI_MinionOptions[i].SetActive(toEnable);
                }
                break;
        }
    }

    public void UIStateChange(PlayerScript.PlayerState newState)
    {
        SetTopText("");
        buttonSelected = true;
        SetMenuActive(false);
        currentMenu = (int)UI_Menus.Base;
        selectorHidden = true;
        switch (newState)
        {
            case PlayerScript.PlayerState.Comander:
                currentItem = 0;
                UI_BaseOptions[currentItem].GetComponent<Image>().color = UISelected;
                SetMenuActive(true);
                mySelector.transform.position = hiddenPos;
                break;
            case PlayerScript.PlayerState.Power:
                mySelector.transform.position = selectorDefaultPos;
                selectorHidden = false;
                break;
            case PlayerScript.PlayerState.Tower:
                break;
            default:
                Debug.Log("State change not set up:" + newState.ToString());
                break;
        }
    }

    // swap menu
    // reset colours -> disable colour menu option not wanted -> set text
    void MenuSwap(UI_Powers menuWanted)
    {
        SetTopText("");
        switch (menuWanted)
        {
            case UI_Powers.Base:
                foreach (GameObject UI in powersUI)
                {
                    if (UI.GetComponent<Button>() != null && UI.GetComponent<Button>().colors.normalColor != UIColours[(int)UI_ButtonState.Enabled])
                    {
                        Button b = UI.GetComponent<Button>();
                        ColorBlock cb = b.colors;
                        cb.normalColor = UIColours[(int)UI_ButtonState.Enabled];
                        b.colors = cb;
                    }
                }
                if (currentlySelectedUnit == null)
                    SetTopText("Currently selected unit: None");
                else
                    SetTopText("Currently selected unit: " + currentlySelectedUnit.name);
                powersUI[(int)UI_PowerGameObject.Base].SetActive(true);
                powersUI[(int)UI_PowerGameObject.Top].GetComponentInChildren<Text>().text = "Change Camera";
                powersUI[(int)UI_PowerGameObject.Left].GetComponentInChildren<Text>().text = "Unit";
                powersUI[(int)UI_PowerGameObject.Right].GetComponentInChildren<Text>().text = "Power";
                powersUI[(int)UI_PowerGameObject.Bottom].GetComponentInChildren<Text>().text = "Exit Overlord";
                inPowersMenu = true;
                currentUIPowersMenu = UI_Powers.Base;
                break;
            case UI_Powers.Unit:
                if (currentlySelectedUnit == null)
                    SetTopText("Currently selected unit: None");
                else
                    SetTopText("Currently selected unit: " + currentlySelectedUnit.name);
                foreach (GameObject UI in powersUI)
                {
                    if (UI.GetComponent<Button>() != null && UI.GetComponent<Button>().colors.normalColor != UIColours[(int)UI_ButtonState.Enabled])
                    {
                        Button b = UI.GetComponent<Button>();
                        ColorBlock cb = b.colors;
                        cb.normalColor = UIColours[(int)UI_ButtonState.Enabled];
                        b.colors = cb;
                    }
                }
                powersUI[(int)UI_PowerGameObject.Base].SetActive(true);
                if (currentlySelectedUnit == null)
                {
                    Button b = powersUI[(int)UI_PowerGameObject.Top].GetComponent<Button>();
                    ColorBlock cb = b.colors;
                    cb.normalColor = UIColours[(int)UI_ButtonState.Disabled];
                    b.colors = cb;
                }
                powersUI[(int)UI_PowerGameObject.Top].GetComponentInChildren<Text>().text = "Commands";
                powersUI[(int)UI_PowerGameObject.Left].GetComponentInChildren<Text>().text = "Select";
                // priority
                if (currentlySelectedUnit != null && currentlySelectedUnit.GetComponent<ChampController>() != null) // if champ
                {
                    switch (currentlySelectedUnit.GetComponent<ChampController>().myStyle)
                    {
                        case ChampController.ChampStyle.Aggressive:
                            powersUI[(int)UI_PowerGameObject.Right].GetComponentInChildren<Text>().text = "Switch to defensive";
                            break;
                        case ChampController.ChampStyle.Defensive:
                            powersUI[(int)UI_PowerGameObject.Right].GetComponentInChildren<Text>().text = "Switch to aggressive";
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Button b = powersUI[(int)UI_PowerGameObject.Right].GetComponent<Button>();
                    ColorBlock cb = b.colors;
                    cb.normalColor = UIColours[(int)UI_ButtonState.Disabled];
                    b.colors = cb;
                    powersUI[(int)UI_PowerGameObject.Right].GetComponentInChildren<Text>().text = "";
                }
                powersUI[(int)UI_PowerGameObject.Bottom].GetComponentInChildren<Text>().text = "Back";
                inPowersMenu = true;
                currentUIPowersMenu = UI_Powers.Unit;
                break;
            case UI_Powers.Commands:
                if (currentlySelectedUnit == null)
                    SetTopText("Currently selected unit: None");
                else
                    SetTopText("Currently selected unit: " + currentlySelectedUnit.name);
                foreach (GameObject UI in powersUI)
                {
                    if (UI.GetComponent<Button>() != null && UI.GetComponent<Button>().colors.normalColor != UIColours[(int)UI_ButtonState.Enabled])
                    {
                        Button b = UI.GetComponent<Button>();
                        ColorBlock cb = b.colors;
                        cb.normalColor = UIColours[(int)UI_ButtonState.Enabled];
                        b.colors = cb;
                    }
                }
                powersUI[(int)UI_PowerGameObject.Base].SetActive(true);
                powersUI[(int)UI_PowerGameObject.Top].GetComponentInChildren<Text>().text = "Attack";
                powersUI[(int)UI_PowerGameObject.Left].GetComponentInChildren<Text>().text = "Hold";
                powersUI[(int)UI_PowerGameObject.Right].GetComponentInChildren<Text>().text = "Auto";
                powersUI[(int)UI_PowerGameObject.Bottom].GetComponentInChildren<Text>().text = "Back";
                inPowersMenu = true;
                currentUIPowersMenu = UI_Powers.Commands;
                break;
            case UI_Powers.Powers:
                if (currentlySelectedUnit == null)
                    SetTopText("Currently selected unit: None");
                else
                    SetTopText("Currently selected unit: " + currentlySelectedUnit.name);
                foreach (GameObject UI in powersUI)
                {
                    if (UI.GetComponent<Button>() != null && UI.GetComponent<Button>().colors.normalColor != UIColours[(int)UI_ButtonState.Enabled])
                    {
                        Button b = UI.GetComponent<Button>();
                        ColorBlock cb = b.colors;
                        cb.normalColor = UIColours[(int)UI_ButtonState.Enabled];
                        b.colors = cb;
                    }
                }
                powersUI[(int)UI_PowerGameObject.Base].SetActive(true);
                powersUI[(int)UI_PowerGameObject.Top].GetComponentInChildren<Text>().text = "EMP Bomb";
                powersUI[(int)UI_PowerGameObject.Left].GetComponentInChildren<Text>().text = "Repair Tower";
                
                if (currentlySelectedUnit != null && currentlySelectedUnit.GetComponent<TowerController>() != null)
                {
                    if (!currentlySelectedUnit.GetComponent<TowerController>().AffordUpgrade())
                    {
                        Button b = powersUI[(int)UI_PowerGameObject.Right].GetComponent<Button>();
                        ColorBlock cb = b.colors;
                        cb.normalColor = UIColours[(int)UI_ButtonState.Disabled];
                        b.colors = cb;
                    }
                    if (currentlySelectedUnit.GetComponent<TowerController>().UpgradeCost() != Int32.MaxValue)
                        powersUI[(int)UI_PowerGameObject.Right].GetComponentInChildren<Text>().text = "Upgrade\n(Cr: " + currentlySelectedUnit.GetComponent<TowerController>().UpgradeCost() + ")";
                    else
                        powersUI[(int)UI_PowerGameObject.Right].GetComponentInChildren<Text>().text = "Upgrade\nMax";
                }
                else if (currentlySelectedUnit != null && currentlySelectedUnit.GetComponent<ChampController>() != null)
                {
                    if (!currentlySelectedUnit.GetComponent<ChampController>().AffordUpgrade())
                    {
                        Button b = powersUI[(int)UI_PowerGameObject.Right].GetComponent<Button>();
                        ColorBlock cb = b.colors;
                        cb.normalColor = UIColours[(int)UI_ButtonState.Disabled];
                        b.colors = cb;
                    }
                    if (currentlySelectedUnit.GetComponent<ChampController>().UpgradeCost() != Int32.MaxValue)
                        powersUI[(int)UI_PowerGameObject.Right].GetComponentInChildren<Text>().text = "Upgrade\n(Cr: " + currentlySelectedUnit.GetComponent<ChampController>().UpgradeCost() + ")";
                    else
                        powersUI[(int)UI_PowerGameObject.Right].GetComponentInChildren<Text>().text = "Upgrade\nMax";
                }
                else
                {
                    Button b = powersUI[(int)UI_PowerGameObject.Right].GetComponent<Button>();
                    ColorBlock cb = b.colors;
                    cb.normalColor = UIColours[(int)UI_ButtonState.Disabled];
                    b.colors = cb;

                    powersUI[(int)UI_PowerGameObject.Right].GetComponentInChildren<Text>().text = "Upgrade";
                }
                powersUI[(int)UI_PowerGameObject.Bottom].GetComponentInChildren<Text>().text = "Back";
                inPowersMenu = true;
                currentUIPowersMenu = UI_Powers.Powers;
                break;
            case UI_Powers.Select:
                if (currentlySelectedUnit == null)
                    SetTopText("Currently selected unit: None");
                else
                    SetTopText("Currently selected unit: " + currentlySelectedUnit.name);
                foreach (GameObject UI in powersUI)
                {
                    if (UI.GetComponent<Button>() != null && UI.GetComponent<Button>().colors.normalColor != UIColours[(int)UI_ButtonState.Enabled])
                    {
                        Button b = UI.GetComponent<Button>();
                        ColorBlock cb = b.colors;
                        cb.normalColor = UIColours[(int)UI_ButtonState.Enabled];
                        b.colors = cb;
                    }
                }
                powersUI[(int)UI_PowerGameObject.Base].SetActive(true);
                powersUI[(int)UI_PowerGameObject.Top].GetComponentInChildren<Text>().text = "";
                if (true) // so the vars get cleaned up onces there used, good old garbage cleanup
                {
                    Button b = powersUI[(int)UI_PowerGameObject.Top].GetComponent<Button>();
                    ColorBlock cb = b.colors;
                    cb.normalColor = UIColours[(int)UI_ButtonState.Disabled];
                    b.colors = cb;
                }
                if (thePlayer.GetComponent<ChampManager>().SpawnedChamps.Count < 1)
                {
                    Button b = powersUI[(int)UI_PowerGameObject.Left].GetComponent<Button>();
                    ColorBlock cb = b.colors;
                    cb.normalColor = UIColours[(int)UI_ButtonState.Disabled];
                    b.colors = cb;
                }
                powersUI[(int)UI_PowerGameObject.Left].GetComponentInChildren<Text>().text = "Next Champion";
                if (thePlayer.GetComponent<BuildManager>().SpawnedTowers.Count < 1)
                {
                    Button b = powersUI[(int)UI_PowerGameObject.Right].GetComponent<Button>();
                    ColorBlock cb = b.colors;
                    cb.normalColor = UIColours[(int)UI_ButtonState.Disabled];
                    b.colors = cb;
                }
                powersUI[(int)UI_PowerGameObject.Right].GetComponentInChildren<Text>().text = "Next Tower";
                powersUI[(int)UI_PowerGameObject.Bottom].GetComponentInChildren<Text>().text = "Cancel";
                inPowersMenu = true;
                currentUIPowersMenu = UI_Powers.Select;
                break;
            case UI_Powers.Attack:
                powersUI[(int)UI_PowerGameObject.Base].SetActive(false);
                inPowersMenu = false;
                currentUIPowersMenu = UI_Powers.Attack;
                break;
            case UI_Powers.Defend:
                powersUI[(int)UI_PowerGameObject.Base].SetActive(false);
                inPowersMenu = false;
                currentUIPowersMenu = UI_Powers.Defend;
                break;
            case UI_Powers.Upgrade:
                powersUI[(int)UI_PowerGameObject.Base].SetActive(false);
                inPowersMenu = false;
                currentUIPowersMenu = UI_Powers.Upgrade;
                break;
            case UI_Powers.Repair:
                powersUI[(int)UI_PowerGameObject.Base].SetActive(false);
                inPowersMenu = false;
                currentUIPowersMenu = UI_Powers.Repair;
                break;
            case UI_Powers.SpecialPower:
                powersUI[(int)UI_PowerGameObject.Base].SetActive(false);
                inPowersMenu = false;
                currentUIPowersMenu = UI_Powers.SpecialPower;
                break;
            default:
                powersUI[(int)UI_PowerGameObject.Base].SetActive(false);
                inPowersMenu = false;
                currentUIPowersMenu = UI_Powers.None;
                break;
        }

    }

    public GameObject GetCurrentlySelected()
    {
        return currentlySelectedUnit;
    }

    public Vector3 GetSelectorPos()
    {
        return mySelector.transform.position;
    }

    public void SetTopText(string sayString)
    {
        powersUI[(int)UI_PowerGameObject.Text].GetComponent<Text>().text = sayString;
    }
}

