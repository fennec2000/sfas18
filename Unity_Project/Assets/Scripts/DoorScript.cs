﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorScript : MonoBehaviour, IDamageable
{
    public int myHealthMax;
    public StartingScript.Team myTeam;
    public Image Healthbar;
    public GameObject theGameManager;

    private int myHealth;

    // Use this for initialization
    void Start()
    {
        myHealth = myHealthMax;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TakeDamage(int damage)
    {
        myHealth -= damage;

        if (!this.gameObject.GetComponentInChildren<Canvas>().enabled)
            this.gameObject.GetComponentInChildren<Canvas>().enabled = true;

        Healthbar.fillAmount = myHealth / (float)myHealthMax;

        if (0 >= myHealth)
        {
            if (myTeam == StartingScript.Team.Team1)
                theGameManager.GetComponent<StartingScript>().EndTheGame(StartingScript.Team.Team2);
            else
                theGameManager.GetComponent<StartingScript>().EndTheGame(StartingScript.Team.Team1);

            Destroy(this.gameObject);
        }
    }
}
