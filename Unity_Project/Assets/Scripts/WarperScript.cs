﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarperScript : MonoBehaviour
{
    private AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public void WarpSound()
    {
        source.Stop();
        source.Play();
    }
}
