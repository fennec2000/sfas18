﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiAllignScript : MonoBehaviour
{
    public GameObject PlayerCamera;

    void Update()
    {
        if (PlayerCamera != null)
            gameObject.GetComponent<RectTransform>().LookAt(PlayerCamera.transform);
    }
}
