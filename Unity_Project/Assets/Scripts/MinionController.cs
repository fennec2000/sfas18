﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public enum TargetType { Tower, Door, NumOfTargetTypes };
enum MinionAudioSources { Move, Attack, Hit, NumOfAudioSources };

public struct EnemyInfo
{
    public TargetType myTargetType;
    public GameObject targetObj;
    public float targetDist;
}

public class MinionController : MonoBehaviour, IDamageable, ICombat
{
    private GameObject myEnemy;
    private MinionSpawner.MinionState myState;
    private Vector3 m_MovementDirection;
    private Vector3 m_CurrentMovementOffset;
    private int currentTarget;
    private Vector3[] myWaypoints;
    private CharacterController m_CharacterController;
    private float m_VerticalSpeed;
    private GameObject targetDoor;
    private float currentAttackTime;
    private int myHealth;
    private int myHealthMax;
    private int myDamage;
    private AudioSource[] source;
    private EnemyInfo currentEnemyInfo;
    private int myUpgrade;

    public StartingScript.Team myTeam;
    public float m_Gravity;
    public float m_MaxFallSpeed;
    public float m_RunSpeed;
    public GameObject GameManager;
    public float hitWaypointRad;
    public float targetRad;
    public float attackSpeed;
    public float attackRange;
    public GameObject[] HealthbarCanvas;
    public Image[] HealthbarImage;
    public float pushedForceMultiplier;
    public float pushedDistance;
    public AudioClip minionMoveSound;
    public AudioClip minionAttackSound;
    public AudioClip minionHurtSound;
    public float minionAttackVol;
    public float minionHitVol;
    public float minionMoveVol;
    public int myBaseHealth;
    public int myBaseDamage;


    private void Awake()
    {
        // Set up audio sources
        AudioMixer mixer = Resources.Load("Audio/MasterMixer") as AudioMixer;
        source = new AudioSource[(int)MinionAudioSources.NumOfAudioSources];

        for (int i = 0; i < (int)MinionAudioSources.NumOfAudioSources; ++i)
        {
            source[i] = gameObject.AddComponent<AudioSource>();
            source[i].playOnAwake = false;
            source[i].Stop();
            source[i].outputAudioMixerGroup = mixer.FindMatchingGroups("Effects")[0];
        }
        // get
        m_CharacterController = GetComponent<CharacterController>();
    }

    // Use this for initialization
    void Start()
    {
        // settings
        currentTarget = 0;
        m_MovementDirection = Vector3.zero;
        m_CurrentMovementOffset = Vector3.zero;
        m_VerticalSpeed = 0.0f;
        myState = MinionSpawner.MinionState.Walking;
        currentAttackTime = 0.0f;
        myHealthMax = myBaseHealth;
        myHealth = myHealthMax;
        myDamage = myBaseDamage;
        myUpgrade = 1;
        currentEnemyInfo = new EnemyInfo();

        for (int i = 0; i < (int)StartingScript.Team.NumOfTeams; ++i)
        {
            HealthbarCanvas[i].SetActive(false);
        }

        HealthbarCanvas[0].GetComponent<GuiAllignScript>().PlayerCamera = GameManager.GetComponent<StartingScript>().PlayerCameras[0];
        HealthbarCanvas[1].GetComponent<GuiAllignScript>().PlayerCamera = GameManager.GetComponent<StartingScript>().PlayerCameras[1];

    }

    // Update is called once per frame
    void Update()
    {
        // when the game is in the playing state
        if (GameManager.GetComponent<StartingScript>().theGameState == StartingScript.GameState.Playing)
        {
            // hit waypoint get next
            if (myWaypoints.Length > currentTarget && Vector3.Distance(myWaypoints[currentTarget], transform.position) <= hitWaypointRad)
                ++currentTarget;

            currentEnemyInfo = TowerCheck(); // is there a tower close

            // attack or walk
            if (currentEnemyInfo.targetObj == null)
                myState = MinionSpawner.MinionState.Walking;
            else
                myState = MinionSpawner.MinionState.Attacking;
            if (currentAttackTime > 0.0f)
                currentAttackTime -= Time.deltaTime;

            switch (myState)
            {
                case MinionSpawner.MinionState.Attacking:
                    if (!source[(int)MinionAudioSources.Attack].isPlaying)
                        source[(int)MinionAudioSources.Attack].PlayOneShot(minionAttackSound, minionAttackVol);
                    Attack();
                    break;
            }

            // calc movement
            UpdateMovementState();
            GetPushed();
            ApplyGravity();
            m_CurrentMovementOffset = (m_MovementDirection * m_RunSpeed + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime;

            // rotate
            if (m_MovementDirection != Vector3.zero)
            {
                RotateCharacter(m_MovementDirection);
            }
            
            // move sound
            if (!source[(int)MinionAudioSources.Move].isPlaying)
                source[(int)MinionAudioSources.Move].PlayOneShot(minionMoveSound, minionMoveVol);

            // move
            m_CharacterController.Move(m_CurrentMovementOffset);
        }
    }

    // Sets varibles from parent to prevent looking
    public void SetGameManager(GameObject man)
    {
        GameManager = man;
        switch(myTeam)
        {
            case StartingScript.Team.Team1:
                myWaypoints = new Vector3[GameManager.GetComponent<MinionSpawner>().Team1Waypath.Length];
                myWaypoints = GameManager.GetComponent<MinionSpawner>().Team1Waypath;
                targetDoor = GameObject.Find("DoorP2");
                break;
            case StartingScript.Team.Team2:
                myWaypoints = new Vector3[GameManager.GetComponent<MinionSpawner>().Team2Waypath.Length];
                myWaypoints = GameManager.GetComponent<MinionSpawner>().Team2Waypath;
                targetDoor = GameObject.Find("DoorP1");
                break;
        }
    }

    // set the minions team
    public void SetMinionTeam(StartingScript.Team givenTeam)
    {
        myTeam = givenTeam;

        switch (myTeam)
        {
            case StartingScript.Team.Team1:
                myEnemy = GameObject.Find("Player2");
                break;
            case StartingScript.Team.Team2:
                myEnemy = GameObject.Find("Player1");
                break;
        }
    }

    public void Attack()
    {
        if (currentAttackTime <= 0.0f)
        {
            switch(currentEnemyInfo.myTargetType)
            {
                case TargetType.Tower:
                    currentEnemyInfo.targetObj.GetComponent<TowerController>().TakeDamage(myDamage);
                    break;
                case TargetType.Door:
                    currentEnemyInfo.targetObj.GetComponent<DoorScript>().TakeDamage(myDamage);
                    break;
            }
            currentAttackTime = attackSpeed;
        }
    }

    // update what to move to
    void UpdateMovementState()
    {
        switch(myState)
        {
            case MinionSpawner.MinionState.Walking: 
                if (myWaypoints.Length <= currentTarget) // final waypoint go to door
                {
                    m_MovementDirection = targetDoor.transform.position - transform.position; 
                }
                else // move to waypoint
                {
                    m_MovementDirection = myWaypoints[currentTarget] - transform.position;
                }
                break;

            case MinionSpawner.MinionState.Attacking:
                m_MovementDirection = currentEnemyInfo.targetObj.transform.position - transform.position;
                break;
        }
        m_MovementDirection.Normalize();
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        // look at the direction we are walking
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(movementDirection.x, 0.0f, movementDirection.z));
        if (transform.rotation != lookRotation)
        {
            transform.rotation = lookRotation;
        }
    }

    void ApplyGravity()
    {
        // Apply gravity
        m_VerticalSpeed -= m_Gravity * Time.deltaTime;

        // Make sure we don't fall any faster than m_MaxFallSpeed.
        m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -m_MaxFallSpeed);
        m_VerticalSpeed = Mathf.Min(m_VerticalSpeed, m_MaxFallSpeed);
    }

    EnemyInfo TowerCheck()
    {
        // find nearest tower with a range
        EnemyInfo foundTarget = new EnemyInfo();

        foreach(GameObject tower in myEnemy.GetComponent<BuildManager>().GetSpawnedTowers()) // each tower
        {
            float newTargetTowerDist = Vector3.Distance(transform.position, tower.transform.position);

            if (newTargetTowerDist < targetRad) // in range
            {
                if (foundTarget.targetObj == null || newTargetTowerDist < foundTarget.targetDist)
                {
                    foundTarget.targetDist = newTargetTowerDist;
                    foundTarget.targetObj = tower;
                    foundTarget.myTargetType = TargetType.Tower;
                }
            }
        }

        // no target set door if close
        if (foundTarget.targetObj == null && targetDoor != null)
        {
            float doorDist = Vector3.Distance(transform.position, targetDoor.transform.position);

            if (doorDist <= targetRad)
            {
                foundTarget.targetDist = doorDist;
                foundTarget.targetObj = targetDoor;
                foundTarget.myTargetType = TargetType.Door;
            }
        }

        return foundTarget;
    }

    public void TakeDamage(int damage)
    {
        // sound
        if (!source[(int)MinionAudioSources.Hit].isPlaying)
            source[(int)MinionAudioSources.Hit].PlayOneShot(minionHurtSound, minionHitVol);
        // damage
        myHealth -= damage;
        // reveal hp bar
        if (HealthbarCanvas[0].activeSelf == false && myHealth < myHealthMax)
        {
            for (int i = 0; i < (int)StartingScript.Team.NumOfTeams; ++i)
            {
                HealthbarCanvas[i].SetActive(true);
            }
        }

        foreach (Image hpImage in HealthbarImage)
        {
            hpImage.fillAmount = myHealth / (float)myHealthMax;
        }

        if (0 >= myHealth)
        {
            GameManager.GetComponent<MinionSpawner>().RemoveMinion(gameObject);
            myEnemy.GetComponent<PlayerScript>().UpdateMoney(myUpgrade);
            Destroy(gameObject);
        }
    }

    // stop minions and champs going into each other
    void GetPushed()
    {
        if (myEnemy.GetComponent<ChampManager>().SpawnedChamps.Count > 0)
        {
            Vector3 pushedForce = new Vector3();

            foreach (GameObject enemyChamps in myEnemy.GetComponent<ChampManager>().SpawnedChamps)
            {
                if (enemyChamps != null)
                {
                    float enemyDist = Vector3.Distance(enemyChamps.transform.position, transform.position);
                    if (enemyDist <= pushedDistance) // enemy too close
                    {
                        pushedForce += (enemyChamps.transform.position - transform.position) / enemyDist; // push away
                    }
                }
            }

            m_MovementDirection += pushedForce.normalized * pushedForceMultiplier;
        }
    }

    public void SetUpgrade(int upgrade)
    {
        myHealthMax = myBaseHealth * upgrade;
        myHealth = myHealthMax;
        myDamage = myBaseDamage;
        myUpgrade = upgrade;
    }
}
