﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Audio;

public class TowerController : MonoBehaviour, IDamageable, ICombat, IUpgradeable, IRepairable
{
    public enum TowerType { Rapid, Bomb, Laser, NumOfTowerTypes}

    public BuildManager myController;
    public float attackRate;
    public StartingScript.Team myTeam;
    public StartingScript.Team myEnemyTeam;
    public float targetRad;
    public Image Healthbar;
    public GameObject forcedTarget;
    public int myBaseDamage;
    public int repairCostPer;
    public float repairTimePer;
    public TowerType myTowerType;
    public int baseMaxHealth;

    private int myHealth;
    private float currentAttackTime;
    private GameObject theGameManager;
    private GameObject theEnemy;
    private GameObject myPlayer;
    private ChampController.UnitUpgrade myUpgrade;
    private int myHealthMax;
    private int myDamage;
    private bool repairing;
    private float repairTimer;
    private int myCost;
    private AudioClip shootSound;
    private AudioSource source;

    // Use this for initialization
    void Start()
    {
        // set up
        myHealth = baseMaxHealth;
        myHealthMax = baseMaxHealth;
        myDamage = myBaseDamage;
        theGameManager = GameObject.Find("GameManager");
        repairing = false;
        repairTimer = 0;
        myUpgrade = ChampController.UnitUpgrade.Default;
        AudioMixer mixer = Resources.Load("Audio/MasterMixer") as AudioMixer;

        switch (myTowerType)
        {
            case TowerType.Rapid:
                gameObject.AddComponent<TowerAttRapid>();
                shootSound = Resources.Load("Audio/Resistance-AssaultRifle_03-Single_Shot-04") as AudioClip;
                source = GetComponent<AudioSource>();
                source.clip = shootSound;
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("Effects")[0];
                break;
            case TowerType.Bomb:
                gameObject.AddComponent<TowerAttBomb>();
                shootSound = Resources.Load("Audio/Metal_Hit_Crash_090") as AudioClip;
                source = GetComponent<AudioSource>();
                source.clip = shootSound;
                source.outputAudioMixerGroup = mixer.FindMatchingGroups("Effects")[0];
                break;
            case TowerType.Laser:
                gameObject.AddComponent<TowerAttLaser>();
                GetComponent<TowerAttLaser>().myRange = targetRad;
                break;
        }

        switch (myTeam)
        {
            case StartingScript.Team.Team1:
                myEnemyTeam = StartingScript.Team.Team2;
                myPlayer = theGameManager.GetComponent<StartingScript>().Player1;
                break;
            case StartingScript.Team.Team2:
                myEnemyTeam = StartingScript.Team.Team1;
                myPlayer = theGameManager.GetComponent<StartingScript>().Player2;
                break;
        }

        if (gameObject.GetComponentInChildren<Canvas>().enabled)
            gameObject.GetComponentInChildren<Canvas>().enabled = false;

        myCost = myPlayer.GetComponent<BuildManager>().towerCosts[(int)myTowerType];
    }

    // Update is called once per frame
    void Update()
    {
        if (0.0f <= currentAttackTime) // attack timer
            currentAttackTime -= Time.deltaTime;

        if (forcedTarget != null && theEnemy != forcedTarget) // attack forced target
            theEnemy = forcedTarget;

        if (forcedTarget != null || EnemyCheck())
            Attack();

        if (repairTimer > 0) // repair timer
            repairTimer -= Time.deltaTime;

        if (repairing && repairTimer <= 0) // repair
        {
            if (myPlayer.GetComponent<PlayerScript>().Afford(repairCostPer) && myHealth < myHealthMax)
            {
                ++myHealth;
                myPlayer.GetComponent<PlayerScript>().UpdateMoney(-repairCostPer);

                if (myHealth >= myHealthMax)
                {
                    repairing = false;
                    myHealth = myHealthMax;
                }
            }
            else
                repairing = false;
        }
    }

    public void TakeDamage(int damage)
    {
        myHealth -= damage;

        if (!gameObject.GetComponentInChildren<Canvas>().enabled)
            gameObject.GetComponentInChildren<Canvas>().enabled = true;

        Healthbar.fillAmount = myHealth / (float)myHealthMax;

        if (0 >= myHealth) // death check
        {
            myController.RemoveTowerFromList(this.gameObject);
            Destroy(this.gameObject);
        }
    }

    public void Attack()
    {
        if (currentAttackTime <= 0)
        {
            currentAttackTime = attackRate;
            switch (myTowerType)
            {
                case TowerType.Rapid:
                    source.Stop();
                    source.Play();
                    gameObject.GetComponent<TowerAttRapid>().RapidAtt(myDamage, theEnemy);
                    break;
                case TowerType.Bomb:
                    source.Stop();
                    source.Play();
                    gameObject.GetComponent<TowerAttBomb>().BombAtt(myDamage, theEnemy, myEnemyTeam, targetRad);
                    break;
                case TowerType.Laser:
                    gameObject.GetComponent<TowerAttLaser>().LaserAtt(myDamage, theEnemy);
                    break;
            }
        }
    }


    bool EnemyCheck() // check for minions
    {
        foreach (GameObject enemy in theGameManager.GetComponent<MinionSpawner>().GetMinionList(myEnemyTeam))
        {
            float newTargetTowerDist = Vector3.Distance(transform.position, enemy.transform.position);

            if (newTargetTowerDist < targetRad)
            {
                theEnemy = enemy;
                return true;
            }
        }
        return false;
    }

    public bool AffordUpgrade()
    {
        if (myUpgrade < ChampController.UnitUpgrade.NumOfUnitUpgrades - 1)
            return myPlayer.GetComponent<PlayerScript>().money >= ((int)myUpgrade + 1) * myCost && myUpgrade < ChampController.UnitUpgrade.NumOfUnitUpgrades;
        return false;
    }

    public int UpgradeCost()
    {
        if (myUpgrade < ChampController.UnitUpgrade.NumOfUnitUpgrades - 1)
            return ((int)myUpgrade + 1) * myCost;
        return Int32.MaxValue;
    }

    public void UpgradeUnit()
    {
        if (AffordUpgrade())
        {
            myPlayer.GetComponent<PlayerScript>().UpdateMoney(-((int)myUpgrade + 1) * myCost);
            ++myUpgrade;
            myHealthMax = baseMaxHealth * (int)myUpgrade;
            myHealth = myHealthMax;
            myDamage = myBaseDamage * (int)myUpgrade;
            transform.position += new Vector3(0, 1, 0);
        }
        else
            Debug.Log("Upgrade failed: " + gameObject.name);
    }

    public void ToggleRepair()
    {
        repairing = !repairing;
    }
}
