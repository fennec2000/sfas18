﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMPBombScript : MonoBehaviour
{
    public float warningTimer;
    public float expolsionTime;
    public float scale;
    public int myDamage;
    public GameObject theGameManager;
    public StartingScript.Team teamToAttack;

    private Color warningColour;
    private Color empColour;
    private float timeAlive;
    private bool boomSwap;

    // Use this for initialization
    void Start()
    {
        warningColour = new Color(1, 0.6f, 0.6f, 0.6f);
        empColour = new Color(0.6f, 0.6f, 1, 1);
        boomSwap = false;
        transform.localScale = new Vector3(scale, 0.01f, scale);
        GetComponent<Renderer>().material.color = warningColour;
    }

    public void Setup(GameObject giveGameMan, StartingScript.Team givenTeam, Vector3 pos)
    {
        theGameManager = giveGameMan;
        teamToAttack = givenTeam;
        transform.position = pos;
    }

    // Update is called once per frame
    void Update()
    {
        timeAlive += Time.deltaTime;
        if (timeAlive >= warningTimer)
        {
            if (!boomSwap)
            {
                boomSwap = true;
                GetComponent<Renderer>().material.color = empColour;
            }

            float size = (timeAlive - warningTimer) / expolsionTime;
            if (size >= 1)
            {
                foreach (GameObject unit in theGameManager.GetComponent<StartingScript>().FindAllClose(gameObject, teamToAttack, scale))
                {
                    if (unit.GetComponent<ChampController>() != null)
                    {
                        unit.GetComponent<ChampController>().TakeDamage(myDamage);
                    }
                    else if (unit.GetComponent<TowerController>() != null)
                    {
                        unit.GetComponent<TowerController>().TakeDamage(myDamage);
                    }
                    else if (unit.GetComponent<MinionController>() != null)
                    {
                        unit.GetComponent<MinionController>().TakeDamage(myDamage);
                    }
                }
                Destroy(gameObject);
            }
            else if (size < 1 && size > 0)
            {
                float currentScale = size * scale;
                transform.localScale = new Vector3(currentScale, currentScale, currentScale);
            }
        }
    }
}
