﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public enum CameraMode { ThirdPerson, RTS, NumOfCameraModes };

    public GameObject GameManger;
    public Vector3 CameraComPickDefaultPos;
    public Vector3 CameraComPickDefaultRot;
    public Vector3 CameraDefaultPos;
    public Vector3 CameraThirdPersonDefaultRot;
    public Vector3 CameraThirdPersonOffset;
    public Vector3 CameraRTSDefaultRot;
    public Vector3 CameraRTSOffset;
    public GameObject myPlayer;
    public GameObject myCanvas;
    public CameraMode myCameraMode;

    private Quaternion CameraComQuad;
    private Quaternion CameraThirdPersonQuad;
    private Quaternion CameraRTSQuad;

    // Use this for initialization
    void Awake ()
    {
        CameraComQuad = Quaternion.Euler(CameraComPickDefaultRot);
        CameraThirdPersonQuad = Quaternion.Euler(CameraThirdPersonDefaultRot);
        CameraRTSQuad = Quaternion.Euler(CameraRTSDefaultRot);
        myCameraMode = CameraMode.ThirdPerson;
    }
	
	// Update is called once per frame
	void Update ()
    {
        switch (myPlayer.GetComponent<PlayerScript>().currentState)
        {
            case PlayerScript.PlayerState.Tower:
                transform.position = myPlayer.GetComponent<BuildManager>().GetPlacerTowerPos() + CameraThirdPersonOffset;
                break;
            case PlayerScript.PlayerState.Power:
                switch(myCameraMode)
                {
                    case CameraMode.ThirdPerson:
                        transform.position = myCanvas.GetComponent<UIController>().GetSelectorPos() + CameraThirdPersonOffset;
                        break;
                    case CameraMode.RTS:
                        transform.position = myCanvas.GetComponent<UIController>().GetSelectorPos() + CameraRTSOffset;
                        break;
                }
                break;
            default:
                break;
        }
    }

    public void ChangeCamera(PlayerScript.PlayerState toState)
    {
        switch (toState)
        {
            case PlayerScript.PlayerState.Comander:
                transform.SetPositionAndRotation(CameraComPickDefaultPos, CameraComQuad);
                break;
            case PlayerScript.PlayerState.Tower:
                transform.SetPositionAndRotation(myCanvas.GetComponent<UIController>().GetSelectorPos() + CameraThirdPersonOffset, CameraThirdPersonQuad);
                break;
            case PlayerScript.PlayerState.Power:
                switch (myCameraMode)
                {
                    case CameraMode.ThirdPerson:
                        transform.SetPositionAndRotation(myCanvas.GetComponent<UIController>().GetSelectorPos() + CameraThirdPersonOffset, CameraThirdPersonQuad);
                        break;
                    case CameraMode.RTS:
                        transform.SetPositionAndRotation(myCanvas.GetComponent<UIController>().GetSelectorPos() + CameraRTSOffset, CameraRTSQuad);
                        break;
                }
                break;
        }
    }

    public void ChangeCameraMode()
    {
        ++myCameraMode;
        if (myCameraMode == CameraMode.NumOfCameraModes)
            myCameraMode = 0;

        switch (myCameraMode)
        {
            case CameraMode.ThirdPerson:
                transform.SetPositionAndRotation(myCanvas.GetComponent<UIController>().GetSelectorPos() + CameraThirdPersonOffset, CameraThirdPersonQuad);
                break;
            case CameraMode.RTS:
                transform.SetPositionAndRotation(myCanvas.GetComponent<UIController>().GetSelectorPos() + CameraRTSOffset, CameraRTSQuad);
                break;
        }
    }
}
