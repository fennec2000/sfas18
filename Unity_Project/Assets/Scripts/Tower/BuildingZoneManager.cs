﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingZoneManager : MonoBehaviour
{
    public GameObject PlacerTower;

    public bool inZone;

	// Use this for initialization
	void Start ()
    {
        inZone = false;
	}

    private void OnTriggerEnter(Collider other)
    {
        if (GameObject.ReferenceEquals(other.gameObject, PlacerTower))
        {
            inZone = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (GameObject.ReferenceEquals(other.gameObject, PlacerTower))
        {
            inZone = false;
        }
    }

    public bool IsPlacerInZone()
    {
        return inZone;
    }
}
