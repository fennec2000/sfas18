﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class BuildManager : MonoBehaviour
{
    public GameObject[] towerPrefab;
    public GameObject[] buildingZones;
    public Vector3 defaultTowerPos;
    public Vector3 defaultTowerRot;
    public float moveSpeed;
    public float collisionDist;
    public List<GameObject> SpawnedTowers;
    public StartingScript.Team myTeam;
    public GameObject myPlayersUI;
    public int[] towerCosts;
    public float[] mapSize;

    private Quaternion defaultTowerQuat;
    private GameObject PlacerTower;
    private bool isTowerPlacerHidden;
    private bool isBuildable;
    private bool placerTowerSpawned;
    private Color towerColour;
    private GameObject theGameManager;
    private GameObject[] theCameras;
    private GameObject myPlayer;

    void Awake()
    {

    }

    void Start()
    {
        // setup
        placerTowerSpawned = false;
        defaultTowerQuat = Quaternion.Euler(defaultTowerRot);
        SpawnedTowers = new List<GameObject>();
        isBuildable = false;
        theGameManager = GameObject.Find("GameManager");
        theCameras = theGameManager.GetComponent<StartingScript>().PlayerCameras;
        switch (myTeam)
        {
            case StartingScript.Team.Team1:
                myPlayer = theGameManager.GetComponent<StartingScript>().Player1;
                break;
            case StartingScript.Team.Team2:
                myPlayer = theGameManager.GetComponent<StartingScript>().Player2;
                break;
        }
    }

    void Update()
    {

        if (PlayerScript.PlayerState.Tower == GetComponent<PlayerScript>().currentState) // if in tower mode
        {
            if (!placerTowerSpawned) // spawn placer if it doesnt exist
            {
                placerTowerSpawned = true;
                PlacerTower = Instantiate(towerPrefab[(int)myPlayersUI.GetComponent<UIController>().currentItem], defaultTowerPos, defaultTowerQuat);
                towerColour = PlacerTower.GetComponent<Renderer>().material.color;
                PlacerTower.name = "PlacerTower" + (int)myTeam;
                foreach (GameObject buildZone in buildingZones)
                {
                    buildZone.GetComponent<BuildingZoneManager>().PlacerTower = PlacerTower;
                }
            }

            // is the tower buildable?
            bool buildableRange = IsBuildableRange(myTeam);
            bool afford = myPlayer.GetComponent<PlayerScript>().Afford(towerCosts[myPlayersUI.GetComponent<UIController>().currentItem]);
            bool towersClose = TowersTooClose();

            // tell the player why
            if (!afford)
                myPlayersUI.GetComponent<UIController>().SetTopText("Can't afford the tower.");
            else if (!buildableRange)
                myPlayersUI.GetComponent<UIController>().SetTopText("Building off building grounds.");
            else if (towersClose)
                myPlayersUI.GetComponent<UIController>().SetTopText("Towers too close.");
            else
                myPlayersUI.GetComponent<UIController>().SetTopText("");

            // make it obvious to the player if can or cant
            if (buildableRange && !towersClose && afford)
            {
                isBuildable = true;
                PlacerTower.GetComponent<MeshRenderer>().material.color = towerColour;
                foreach (Transform child in PlacerTower.transform)
                {
                    if (child.GetComponent<MeshRenderer>() != null)
                        child.GetComponent<MeshRenderer>().material.color = towerColour;
                }
            }
            else
            {
                isBuildable = false;
                PlacerTower.GetComponent<MeshRenderer>().material.color = Color.red;
                foreach (Transform child in PlacerTower.transform)
                {
                    if (child.GetComponent<MeshRenderer>() != null)
                        child.GetComponent<MeshRenderer>().material.color = Color.red;
                }
            }

            // controls
            float x, z;
            bool fireDown;

            switch (myTeam)
            {
                case StartingScript.Team.Team1:
                    x = Input.GetAxis("Horizontal_P1") * Time.deltaTime * moveSpeed;
                    z = Input.GetAxis("Vertical_P1") * Time.deltaTime * moveSpeed;
                    fireDown = Input.GetButtonDown("Fire_P1");
                    break;
                case StartingScript.Team.Team2: // player 2 is fliped on the world, so controls need flipping
                    x = Input.GetAxis("Horizontal_P2") * Time.deltaTime * -moveSpeed;
                    z = Input.GetAxis("Vertical_P2") * Time.deltaTime * -moveSpeed;
                    fireDown = Input.GetButtonDown("Fire_P2");
                    break;
                default:
                    x = 0.0f;
                    z = 0.0f;
                    fireDown = false;
                    break;
            }

            // check if out of map else move
            Vector3 newPos = new Vector3(-z, 0, x);
            if ((PlacerTower.transform.position + newPos).x < mapSize[0] && (PlacerTower.transform.position + newPos).x > mapSize[2] &&
                        (PlacerTower.transform.position + newPos).z < mapSize[1] && (PlacerTower.transform.position + newPos).z > mapSize[3])
                PlacerTower.transform.Translate(-z, 0, x);
            if (fireDown)
            {
                if (isBuildable) // build the toewr
                {
                    myPlayer.GetComponent<PlayerScript>().UpdateMoney(-towerCosts[myPlayersUI.GetComponent<UIController>().currentItem]);
                    PlacerTower.transform.parent = this.transform;
                    PlacerTower.GetComponent<CapsuleCollider>().isTrigger = false; // add colision
                    PlacerTower.GetComponent<TowerController>().enabled = true; // turn on
                    PlacerTower.GetComponent<TowerController>().myTowerType = (TowerController.TowerType)myPlayersUI.GetComponent<UIController>().currentItem; // tell the tower its type
                    PlacerTower.name = PlacerTower.GetComponent<TowerController>().myTowerType.ToString(); // set name
                    PlacerTower.GetComponent<TowerController>().myController = this;
                    PlacerTower.GetComponent<TowerController>().myTeam = myTeam; // set team

                    // set the player cameras for the health bar to point at
                    for(int i = 0; i < PlacerTower.transform.childCount; ++i)
                    {
                        if (PlacerTower.transform.GetChild(i).GetComponent<GuiAllignScript>() != null)
                            PlacerTower.transform.GetChild(i).GetComponent<GuiAllignScript>().PlayerCamera = theCameras[i];
                    }
                    foreach (GameObject buildZone in buildingZones) // reset placer tower in zones
                        buildZone.GetComponent<BuildingZoneManager>().PlacerTower = null;

                    // give enemy team
                    switch (myTeam)
                    {
                        case StartingScript.Team.Team1:
                            PlacerTower.GetComponent<TowerController>().myEnemyTeam = StartingScript.Team.Team2;
                            break;
                        case StartingScript.Team.Team2:
                            PlacerTower.GetComponent<TowerController>().myEnemyTeam = StartingScript.Team.Team1;
                            break;
                    }
                    SpawnedTowers.Add(PlacerTower); // add to list of towers
                                                    // back to commander
                    GetComponent<PlayerScript>().SetState(PlayerScript.PlayerState.Comander);
                    placerTowerSpawned = false;

                    // update defencive targets on champs
                    foreach (GameObject champ in GetComponent<ChampManager>().SpawnedChamps)
                    {
                        champ.GetComponent<ChampController>().RecalcDefenciveTarget();
                    }
                }
                else
                {
                    foreach (GameObject buildZone in buildingZones) // reset placer tower in zones
                        buildZone.GetComponent<BuildingZoneManager>().PlacerTower = null;

                    placerTowerSpawned = false;
                    Destroy(PlacerTower);

                    GetComponent<PlayerScript>().SetState(PlayerScript.PlayerState.Comander);
                }
            }
        }
    }

    public Vector3 GetPlacerTowerPos()
    {
        return PlacerTower.transform.position;
    }

    bool IsBuildableRange(StartingScript.Team team) // it the tower in the buildable zone
    {
        foreach (GameObject buildZone in buildingZones)
        {
            if (buildZone.GetComponent<BuildingZoneManager>().IsPlacerInZone())
                return true;
        }

        return false;
    }

    bool TowersTooClose() // check if tower are too close to each other
    {
        foreach (GameObject tower in SpawnedTowers)
            if (Vector2.Distance(new Vector2(tower.transform.position.x, tower.transform.position.z),
                new Vector2(PlacerTower.transform.position.x, PlacerTower.transform.position.z)) < collisionDist)
                return true;
        return false;
    }

    public List<GameObject> GetSpawnedTowers()
    {
        return SpawnedTowers;
    }

    public void RemoveTowerFromList(GameObject Tower)
    {
        SpawnedTowers.Remove(Tower);
    }

    public GameObject GetNextTower(GameObject lastTower = null) // find the next tower in the list and return it
    {
        if (SpawnedTowers.Count <= 0)
            return null;

        if (lastTower == null || !SpawnedTowers.Contains(lastTower) || SpawnedTowers[SpawnedTowers.Count - 1] == lastTower)
            return SpawnedTowers[0];

        return SpawnedTowers[SpawnedTowers.IndexOf(lastTower) + 1];
    }
}
