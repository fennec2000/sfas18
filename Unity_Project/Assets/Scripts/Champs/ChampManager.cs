﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChampManager : MonoBehaviour
{

    public Vector3 champTowerPos;
    public Vector3 champTowerRot;
    public GameObject[] ChampPrefabs;
    public GameObject theGameManager;
    public int[] champCosts;

    private Quaternion champTowerQuat;
    private bool[] AliveChamps;
    public List<GameObject> SpawnedChamps;
    private GameObject PlayerUI;

    // Use this for initialization
    void Start()
    {
        champTowerQuat = Quaternion.Euler(champTowerRot);
        AliveChamps = new bool[3];
        SpawnedChamps = new List<GameObject>();
        PlayerUI = GetComponent<PlayerScript>().myUI;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SpawnChamp(ChampController.ChampType type, ChampController.UnitUpgrade upgrade = ChampController.UnitUpgrade.Default)
    {
        Debug.Assert(type >= 0 && type < ChampController.ChampType.NumOfChampTypes, "Champion spawner outside of champion types.");

        if (CanSpawn(type))
        {

            GetComponent<PlayerScript>().UpdateMoney(-champCosts[PlayerUI.GetComponent<UIController>().currentItem]);
            AliveChamps[(int)type] = true;
            GameObject newChamp = Instantiate(ChampPrefabs[(int)type], champTowerPos, champTowerQuat);
            newChamp.GetComponent<ChampController>().ChampSetup(type, upgrade);
            newChamp.name = type.ToString();
            newChamp.transform.parent = gameObject.transform;
            newChamp.transform.Find("Canvas").GetComponent<GuiAllignScript>().PlayerCamera = theGameManager.GetComponent<StartingScript>().PlayerCameras[(int)gameObject.GetComponent<PlayerScript>().myTeam];
            SpawnedChamps.Add(newChamp);
        }
    }

    public void KillChamp(GameObject dyingChamp)
    {
        AliveChamps[(int)dyingChamp.GetComponent<ChampController>().GetChampType()] = false;

        Destroy(dyingChamp);
    }

    public GameObject GetNextChamp(GameObject lastChamp = null)
    {
        if (SpawnedChamps.Count <= 0)
            return null;
        
        if (lastChamp == null || !SpawnedChamps.Contains(lastChamp) || SpawnedChamps[SpawnedChamps.Count - 1] == lastChamp)
            return SpawnedChamps[0];

        return SpawnedChamps[SpawnedChamps.IndexOf(lastChamp) + 1];
    }

    public bool CanSpawn(ChampController.ChampType type)
    {
        return !AliveChamps[(int)type] && GetComponent<PlayerScript>().Afford(champCosts[(int)type]);
    }
}
