﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAllignmentScript : MonoBehaviour
{
    public GameObject myCamera;

    // Use this for initialization
    void Start()
    {
        transform.position = myCamera.transform.position;
        transform.rotation = myCamera.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
