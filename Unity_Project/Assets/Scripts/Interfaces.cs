﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable
{
    void TakeDamage(int damage);
}

public interface ICombat
{
    void Attack();
}

public interface IUpgradeable
{
    bool AffordUpgrade();
    void UpgradeUnit();
}

public interface IRepairable
{
    void ToggleRepair();
}