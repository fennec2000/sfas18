﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public enum PlayerState { Comander, Tower, Minion, Power, Dead, NumOfPlayerStates };
    public PlayerState currentState;
    public GameObject theGameManager;
    public GameObject myCamera;
    public GameObject myUI;
    public StartingScript.Team myTeam;
    public GameObject myMoneyText;
    public int money;

    private StartingScript.Team myEnemy;

    // Use this for initialization
    void Start ()
    {
        switch(myTeam)
        {
            case StartingScript.Team.Team1:
                myEnemy = StartingScript.Team.Team2;
                break;
            case StartingScript.Team.Team2:
                myEnemy = StartingScript.Team.Team1;
                break;
        }
        SetState(PlayerState.Comander);
        UpdateMoney(0);
    }
	
	// Update is called once per frame
	void Update ()
    {

    }

    public void SetState(PlayerState newState)
    {
        currentState = newState;
        myUI.GetComponent<UIController>().UIStateChange(newState);
        myCamera.GetComponent<CameraController>().ChangeCamera(newState);
    }

    public void UpdateMoney(int moneyChange)
    {
        if (money + moneyChange < 0)
        {
            Debug.Log("Money out of bounds");
            return;
        }
        money += moneyChange;
        myMoneyText.GetComponent<Text>().text = "Credits: " + money;
    }

    public bool Afford(int cost)
    {
        return money - cost >= 0;
    }
}
