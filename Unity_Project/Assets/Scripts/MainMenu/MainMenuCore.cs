﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MainMenuCore : MonoBehaviour
{
    enum MenuState { Default, ReadyRoom, Options, Credits, NumOfMenuStates };
    enum GameType { None, LocalCoop, NumOgGameTypes };
    enum MenuColours { NotSelected, Selected, Ready, NumOfMenuColours };

    private MenuState theMenuState;
    private GameType wantedGame;
    private int menuOptionSelected;
    private Color[] menuColours;
    private bool buttonDownP1;
    private bool buttonDownP2;
    private bool[] playersReady;
    private AudioMixerGroup[] mixerGroups;
    private float[] volumes;
    private string[] volumeNames;

    public GameObject[] mainPageObjs; // Menu buttons need to be top
    public GameObject[] readyPageObjs;
    public GameObject[] creditsPageObjs; // Credits text needs to be the first in the list
    public GameObject[] optionsPageObjs;
    public GameObject[] readyButtons;
    public AudioMixer mixer;
    public GameObject musicHolder;

    public int[] MenuMax;
    public float[] creditsPos;
    public float creditsScrollRate;
    public float volChangeRate;
    public int effectsSliderPos; // number in list from top that the effects slider sits at

    // Use this for initialization
    void Start()
    {
        buttonDownP1 = false;
        theMenuState = MenuState.Default;
        wantedGame = GameType.None;
        playersReady = new bool[] { false, false };
        menuColours = new Color[]
        {
            new Color(1, 1, 1, 1),
            new Color(0.6f, 0.6f, 1, 1),
            new Color(0, 0.6f, 0, 1),
        };
        volumeNames = new string[]
        {
            "Master",
            "Music",
            "Effects",
        };
        mixerGroups = new AudioMixerGroup[volumeNames.Length];
        for (int i = 0; i < volumeNames.Length; ++i)
            mixerGroups[i] = mixer.FindMatchingGroups(volumeNames[i])[0];

        volumes = new float[volumeNames.Length];
        for (int i = 0; i < volumeNames.Length; ++i)
            mixer.GetFloat(volumeNames[i] + "Vol", out volumes[i]);

        ChangeStateChangeMenuState(MenuState.Default);
    }

    // Update is called once per frame
    void Update()
    {
        // input
        float vertInputP1 = Input.GetAxisRaw("Vertical_P1");
        float horInputP1 = Input.GetAxisRaw("Horizontal_P1");
        float vertInputP2 = Input.GetAxisRaw("Vertical_P2");
        float horInputP2 = Input.GetAxisRaw("Horizontal_P2");
        bool fireDownP1 = Input.GetButtonDown("Fire_P1");
        bool fireDownP2 = Input.GetButtonDown("Fire_P2");

        switch (theMenuState)
        {
            case MenuState.Default: // main menu
                if (vertInputP1 > 0 && !buttonDownP1)
                {
                    PrevMenu();
                    buttonDownP1 = true;
                }
                if (vertInputP1 < 0 && !buttonDownP1)
                {
                    NextMenu();
                    buttonDownP1 = true;
                }
                if (fireDownP1 && !buttonDownP1)
                {
                    buttonDownP1 = true;
                    if (menuOptionSelected == MenuMax[(int)MenuState.Default] - 1)
                        Application.Quit();
                    else
                        ChangeStateChangeMenuState((MenuState)(menuOptionSelected + 1));
                }

                if (vertInputP1 == 0 && !fireDownP1)
                    buttonDownP1 = false;
                break;
            case MenuState.ReadyRoom: // ready room player are ready or exit
                if (vertInputP1 > 0 && !buttonDownP1)
                {
                    if (playersReady[0])
                        ReadyPlayer(0);
                    PrevMenu();
                    buttonDownP1 = true;
                }
                if (vertInputP1 < 0 && !buttonDownP1)
                {
                    if (playersReady[0])
                        ReadyPlayer(0);
                    NextMenu();
                    buttonDownP1 = true;
                }
                if (fireDownP1 && !buttonDownP1)
                {
                    switch (menuOptionSelected)
                    {
                        case 0:
                            ReadyPlayer(0);
                            break;
                        case 1:
                            ChangeStateChangeMenuState(MenuState.Default);
                            break;
                    }
                }
                if (fireDownP2 && !buttonDownP2)
                {
                    ReadyPlayer(1);
                    buttonDownP2 = true;
                }

                if (playersReady[0] && playersReady[1]) // both realy launch game
                    LaunchGameLocalCoop();
                break;
            case MenuState.Credits:
                if (!buttonDownP1 && (vertInputP1 != 0 || vertInputP2 != 0 || horInputP1 != 0 || horInputP2 != 0 || fireDownP1 || fireDownP2)) // cancel creditss
                {
                    ChangeStateChangeMenuState(MenuState.Default);
                    break;
                }
                // scroll
                creditsPageObjs[0].transform.position += new Vector3(0, creditsScrollRate, 0);
                if (creditsPageObjs[0].transform.position.y >= creditsPos[1] + Screen.height)
                {
                    ChangeStateChangeMenuState(MenuState.Default);
                }
                break;
            case MenuState.Options: // main menu
                if (vertInputP1 > 0 && !buttonDownP1)
                {
                    if (menuOptionSelected == effectsSliderPos)
                        PlayEffectsSoundsOnly(false);
                    PrevMenu();
                    if (menuOptionSelected == effectsSliderPos)
                        PlayEffectsSoundsOnly(true);
                    buttonDownP1 = true;
                }
                if (vertInputP1 < 0 && !buttonDownP1)
                {
                    if (menuOptionSelected == effectsSliderPos)
                        PlayEffectsSoundsOnly(false);
                    NextMenu();
                    if (menuOptionSelected == effectsSliderPos)
                        PlayEffectsSoundsOnly(true);
                    buttonDownP1 = true;
                }
                if (horInputP1 > 0 && !buttonDownP1)
                {
                    if (optionsPageObjs[menuOptionSelected].GetComponent<Slider>() != null)
                    {
                        optionsPageObjs[menuOptionSelected].GetComponent<Slider>().value += volChangeRate;
                        float currentVol = optionsPageObjs[menuOptionSelected].GetComponent<Slider>().value;
                        mixer.SetFloat(volumeNames[menuOptionSelected] + "Vol", Mathf.Pow(currentVol, 1 / 2.71828f) * 100 - 80);
                    }
                }
                if (horInputP1 < 0 && !buttonDownP1)
                {
                    if (optionsPageObjs[menuOptionSelected].GetComponent<Slider>() != null)
                    {
                        optionsPageObjs[menuOptionSelected].GetComponent<Slider>().value -= volChangeRate;
                        float currentVol = optionsPageObjs[menuOptionSelected].GetComponent<Slider>().value;
                        mixer.SetFloat(volumeNames[menuOptionSelected] + "Vol", Mathf.Pow(currentVol, 1 / 2.71828f) * 100 - 80);
                    }
                }
                if (fireDownP1 && !buttonDownP1)
                {
                    buttonDownP1 = true;
                    if (menuOptionSelected == MenuMax[(int)MenuState.Default] - 1)
                        ChangeStateChangeMenuState(MenuState.Default);
                }

                if (vertInputP1 == 0 && !fireDownP1)
                    buttonDownP1 = false;
                break;
        }

        if (vertInputP1 == 0 && !fireDownP1)
            buttonDownP1 = false;
        if (!fireDownP2)
            buttonDownP2 = false;
    }

    void ReadyPlayer(int player)
    {
        // set the players button and could depening on there state
        playersReady[player] = !playersReady[player];
        Button b = readyButtons[player].GetComponent<Button>();
        ColorBlock cb = b.colors;
        if (playersReady[player])
        {
            readyButtons[player].GetComponentInChildren<Text>().text = "Player " + (player + 1) + "\nReady";
            cb.normalColor = menuColours[(int)MenuColours.Ready];
        }
        else if (player == 0 && menuOptionSelected == 0)
        {
            readyButtons[player].GetComponentInChildren<Text>().text = "Player " + (player + 1) + "\nNot Ready";
            cb.normalColor = menuColours[(int)MenuColours.Selected];
        }
        else
        {
            readyButtons[player].GetComponentInChildren<Text>().text = "Player " + (player + 1) + "\nNot Ready";
            cb.normalColor = menuColours[(int)MenuColours.NotSelected];
        }
        b.colors = cb;
    }

    void UpdateMenuColours()
    {
        // set the colour of the buttons
        switch (theMenuState)
        {
            case MenuState.Default: // main menu
                for (int i = 0; i < mainPageObjs[0].transform.childCount; ++i)
                {
                    if (mainPageObjs[0].transform.GetChild(i).GetComponentInChildren<Button>() != null)
                    {
                        if (menuOptionSelected == i)
                        {
                            Button b = mainPageObjs[0].transform.GetChild(i).GetComponent<Button>();
                            ColorBlock cb = b.colors;
                            cb.normalColor = menuColours[(int)MenuColours.Selected];
                            b.colors = cb;
                        }
                        else
                        {
                            Button b = mainPageObjs[0].transform.GetChild(i).GetComponent<Button>();
                            ColorBlock cb = b.colors;
                            cb.normalColor = menuColours[(int)MenuColours.NotSelected];
                            b.colors = cb;
                        }
                    }
                }
                break;
            case MenuState.ReadyRoom: // ready room
                for (int i = 0; i < readyPageObjs[0].transform.childCount; ++i)
                {
                    if (menuOptionSelected == i)
                    {
                        Button b = readyPageObjs[0].transform.GetChild(i).GetComponent<Button>();
                        ColorBlock cb = b.colors;
                        cb.normalColor = menuColours[(int)MenuColours.Selected];
                        b.colors = cb;
                    }
                    else
                    {
                        Button b = readyPageObjs[0].transform.GetChild(i).GetComponent<Button>();
                        ColorBlock cb = b.colors;
                        cb.normalColor = menuColours[(int)MenuColours.NotSelected];
                        b.colors = cb;
                    }
                }
                break;
            case MenuState.Options: // options
                for (int i = 0; i < optionsPageObjs.Length; ++i)
                {
                    if (optionsPageObjs[i].GetComponent<Slider>() != null)
                    {
                        if (menuOptionSelected == i)
                        {
                            Slider s = optionsPageObjs[i].GetComponent<Slider>();
                            ColorBlock cb = s.colors;
                            cb.normalColor = menuColours[(int)MenuColours.Selected];
                            s.colors = cb;
                        }
                        else
                        {
                            Slider s = optionsPageObjs[i].GetComponent<Slider>();
                            ColorBlock cb = s.colors;
                            cb.normalColor = menuColours[(int)MenuColours.NotSelected];
                            s.colors = cb;
                        }
                    }
                    else if (optionsPageObjs[i].GetComponent<Button>() != null)
                    {
                        if (menuOptionSelected == i)
                        {
                            Button b = optionsPageObjs[i].GetComponent<Button>();
                            ColorBlock cb = b.colors;
                            cb.normalColor = menuColours[(int)MenuColours.Selected];
                            b.colors = cb;
                        }
                        else
                        {
                            Button b = optionsPageObjs[i].GetComponent<Button>();
                            ColorBlock cb = b.colors;
                            cb.normalColor = menuColours[(int)MenuColours.NotSelected];
                            b.colors = cb;
                        }
                    }
                }
                break;
        }
    }

    void NextMenu()
    {
        ++menuOptionSelected;
        if (menuOptionSelected >= MenuMax[(int)theMenuState])
            menuOptionSelected = 0;
        UpdateMenuColours();
    }

    void PrevMenu()
    {
        --menuOptionSelected;
        if (menuOptionSelected < 0)
            menuOptionSelected = MenuMax[(int)theMenuState] - 1;
        UpdateMenuColours();
    }

    void LaunchGameLocalCoop()
    {
        SceneManager.LoadScene("Arena", LoadSceneMode.Single);
    }

    // Change the state and enable or disable the items for that menu
    void ChangeStateChangeMenuState(MenuState newState)
    {
        // disable last menu
        switch (theMenuState)
        {
            case MenuState.Default:
                foreach (GameObject ui in mainPageObjs)
                    ui.SetActive(false);
                break;
            case MenuState.ReadyRoom:
                foreach (GameObject ui in readyPageObjs)
                    ui.SetActive(false);
                break;
            case MenuState.Credits:
                foreach (GameObject ui in creditsPageObjs)
                    ui.SetActive(false);
                break;
            case MenuState.Options:
                foreach (GameObject ui in optionsPageObjs)
                    ui.SetActive(false);
                break;
        }

        menuOptionSelected = 0;
        theMenuState = newState;

        // enable new menu
        switch (theMenuState)
        {
            case MenuState.Default:
                foreach (GameObject ui in mainPageObjs)
                    ui.SetActive(true);
                break;
            case MenuState.ReadyRoom:
                foreach (GameObject ui in readyPageObjs)
                    ui.SetActive(true);
                break;
            case MenuState.Credits:
                foreach (GameObject ui in creditsPageObjs)
                    ui.SetActive(true);
                creditsPageObjs[0].transform.position = new Vector3(Screen.width / 2, creditsPos[0], 0);
                break;
            case MenuState.Options:
                foreach (GameObject ui in optionsPageObjs)
                    ui.SetActive(true);

                for (int i = 0; i < volumeNames.Length; ++i)
                    optionsPageObjs[i].GetComponent<Slider>().value = Mathf.Pow((volumes[i] + 80) / 100, 2.71828f);
                break;
        }
        UpdateMenuColours(); // fix colours
    }

    void PlayEffectsSoundsOnly(bool ans)
    {
        if (ans)
        {
            musicHolder.GetComponent<AudioSource>().Pause();
            GetComponent<AudioSource>().Play();
        }
        else
        {
            GetComponent<AudioSource>().Stop();
            musicHolder.GetComponent<AudioSource>().UnPause();
        }
    }
}
