﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class TowerAttRapid : MonoBehaviour
{
    private Vector3 bulletSpawnPos;
    private Vector3 dirrection;

    private void Start()
    {
        bulletSpawnPos = new Vector3(0, 1, 0);
    }

    public void RapidAtt(int myDamage, GameObject target)
    {
        GameObject bulletObj = Instantiate(Resources.Load<GameObject>("Bullet"), gameObject.transform.position + bulletSpawnPos, new Quaternion());
        bulletObj.transform.parent = gameObject.transform;
        bulletObj.AddComponent<TowerRapidBullet>();
        bulletObj.GetComponent<TowerRapidBullet>().Setup(myDamage, target);
    }
}

public class TowerRapidBullet : MonoBehaviour
{
    private GameObject target;
    private int myDamage;
    private Vector3 dirrection;
    private bool setup;

    private void Start()
    {
        setup = false;
    }

    public void Setup(int damage, GameObject givenTarget)
    {
        setup = true;
        target = givenTarget;
        myDamage = damage;
    }

    public void Update()
    {
        if (target == null && setup)
            Destroy(gameObject);
        
        if (target != null)
        {
            dirrection = target.transform.position - transform.position;
            transform.position += dirrection.normalized * Time.deltaTime;
        }


        if (target != null && Vector3.Distance(target.transform.position, transform.position) < dirrection.magnitude * Time.deltaTime)
        {
            if (target.GetComponent<MinionController>() != null) // attacking minion
                target.GetComponent<MinionController>().TakeDamage(myDamage);
            else if (target.GetComponent<ChampController>() != null) // attacking champ
                target.GetComponent<ChampController>().TakeDamage(myDamage);
            Destroy(gameObject);
        }
    }
}

public class TowerAttBomb : MonoBehaviour
{
    Vector3 vel;
    Vector3 bombSpawnPos;
    float bombFallTime;
    float vertVel;

    private void Start()
    {
        bombSpawnPos = new Vector3(0, 2, 0);
        bombFallTime = 1.0f;
        vertVel = 4.6f;
    }

    public void BombAtt(int damage, GameObject givenTarget, StartingScript.Team teamToAttack, float range)
    {
        GameObject bombObj = Instantiate(Resources.Load<GameObject>("Bomb"), gameObject.transform.position + bombSpawnPos, new Quaternion());
        vel = givenTarget.transform.position - bombObj.transform.position;
        vel.y = 0;
        vel = vel.normalized * Vector3.Distance(givenTarget.transform.position, bombObj.transform.position);
        vel.y = vertVel;
        bombObj.transform.parent = gameObject.transform;
        bombObj.AddComponent<TowerBombBomb>();
        bombObj.GetComponent<TowerBombBomb>().Setup(damage, vel, range, teamToAttack, bombFallTime);
    }
}

public class TowerBombBomb : MonoBehaviour
{
    private GameObject theGameManager;
    private StartingScript.Team EnemyTeam;
    private Vector3 target;
    private Vector3 velocity;
    private Vector3 gravity;
    private int myDamage;
    private float range;
    private float timer;
    private bool setup;

    private void Awake()
    {
        setup = false;
    }

    public void Setup(int givenDamage, Vector3 givenVel, float givenRange, StartingScript.Team teamToAttack, float givenTime)
    {
        gravity = new Vector3(0, -12.0f, 0);
        myDamage = givenDamage;
        velocity = givenVel;
        range = givenRange;
        timer = givenTime;
        EnemyTeam = teamToAttack;
        setup = true;
        theGameManager = GameObject.Find("GameManager");
    }

    private void Update()
    {
        if (timer <= 0 && setup) // hit the floor
        {
            foreach(GameObject unit in theGameManager.GetComponent<StartingScript>().FindAllClose(gameObject, EnemyTeam, range, true, false, true, false))
            {
                if (unit.GetComponent<ChampController>() != null)
                {
                    unit.GetComponent<ChampController>().TakeDamage(myDamage);
                }
                else if (unit.GetComponent<MinionController>() != null)
                {
                    unit.GetComponent<MinionController>().TakeDamage(myDamage);
                }
            }
            Destroy(gameObject);
        }
        else if (setup)
        {
            timer -= Time.deltaTime;
            velocity += gravity * Time.deltaTime;
            transform.position += velocity * Time.deltaTime;
        }
    }
}

public class TowerAttLaser : MonoBehaviour
{
    enum LaserSounds { Ambience, Shoot, NumOfLaserSounds }
    private GameObject cube;
    private GameObject lastTarget;
    private LineRenderer Laser;
    private bool laserOn;
    private float currentAttackTime;
    private float spinUpTime;
    private float spinRate;
    private float spinRateMin;
    private float spinRateMax;
    private float spinAcceleration;
    private AudioClip[] towerSounds;
    private AudioSource[] source;

    public float myRange;
    public float[] myVolumes;

    private void Awake()
    {
        cube = transform.Find("Cube").gameObject;
    }

    private void Start()
    {
        AudioMixer mixer = Resources.Load("Audio/MasterMixer") as AudioMixer;
        myVolumes = new float[(int)LaserSounds.NumOfLaserSounds]
        {
            1, // Ambience
            1, // Shoot
        };
        source = new AudioSource[(int)LaserSounds.NumOfLaserSounds];
        source[(int)LaserSounds.Ambience] = GetComponent<AudioSource>();
        source[(int)LaserSounds.Shoot] = gameObject.AddComponent<AudioSource>();

        towerSounds = new AudioClip[(int)LaserSounds.NumOfLaserSounds];
        towerSounds[(int)LaserSounds.Ambience] = Resources.Load("Audio/PM_CS_hologram_background2") as AudioClip;
        towerSounds[(int)LaserSounds.Shoot] = Resources.Load("Audio/Electrical_Current_MouthNoises_Fienup_001") as AudioClip;

        for (int i = 0; i < (int)LaserSounds.NumOfLaserSounds; ++i)
        {
            source[i].loop = true;
            source[i].playOnAwake = false;
            source[i].Stop();
            source[i].volume = myVolumes[i];
            source[i].clip = towerSounds[i];
            source[i].outputAudioMixerGroup = mixer.FindMatchingGroups("Effects")[0];
        }

        Laser = gameObject.AddComponent<LineRenderer>();
        Laser.material = Resources.Load("Materials/Towers/Laser") as Material;
        Laser.SetPosition(0, cube.transform.position);
        Laser.startWidth = 0.5f;
        Laser.endWidth = 0.2f;
        Laser.enabled = false;
        laserOn = false;
        currentAttackTime = 0;

        spinUpTime = 5.0f;
        spinRateMin = 3.0f;
        spinRateMax = 10.0f;
        spinAcceleration = (spinRateMax - spinRateMin) / spinUpTime;
        spinRate = spinRateMin;

        source[(int)LaserSounds.Ambience].Play();
    }

    void Update()
    {
        cube.transform.Rotate(new Vector3(0, -45, 35.264f) * Time.deltaTime * spinRate);

        if (currentAttackTime > 0)
            currentAttackTime -= Time.deltaTime;

        if (lastTarget == null) // no target slow down
        {
            if (laserOn)
            {
                source[(int)LaserSounds.Shoot].Stop();
                Laser.enabled = false;
                laserOn = false;
            }
            if (spinRate > spinRateMin)
            {
                source[(int)LaserSounds.Ambience].pitch -= spinAcceleration / spinRateMax;
                spinRate -= spinAcceleration;
                if (spinRate <= spinRateMin)
                {
                    source[(int)LaserSounds.Ambience].pitch = 1.0f;
                    spinRate = spinRateMin;
                }
            }
        }
        else
        {
            if (Vector3.Distance(lastTarget.transform.position, transform.position) < myRange)
            {
                if (spinRate < spinRateMax)
                {
                    source[(int)LaserSounds.Ambience].pitch += spinAcceleration / spinRateMax;
                    spinRate += spinAcceleration;
                }
                else
                {
                    Laser.SetPosition(1, lastTarget.transform.position);
                    if (!source[(int)LaserSounds.Shoot].isPlaying)
                        source[(int)LaserSounds.Shoot].Play();
                    Laser.enabled = true;
                    laserOn = true;
                }
                
            }
        }
    }

    public void LaserAtt(int myDamage, GameObject target)
    {
        lastTarget = target;

        if (spinRate + float.Epsilon >= spinRateMax)
        {
            if (lastTarget.GetComponent<MinionController>() != null) // attacking minion
                lastTarget.GetComponent<MinionController>().TakeDamage(myDamage);
            else if (lastTarget.GetComponent<ChampController>() != null) // attacking champ
                lastTarget.GetComponent<ChampController>().TakeDamage(myDamage);
        }
    }
}