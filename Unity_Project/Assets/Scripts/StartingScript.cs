﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartingScript : MonoBehaviour
{
    public enum Team { Team1, Team2, NumOfTeams };
    public enum GameState { PreGame, Playing, PostGame, NumOfGameStates };

    public GameObject Player1;
    public GameObject Player2;
    public GameObject[] PlayerCameras;
    public GameState theGameState;
    public int preGameTime;
    public GameObject theClock;
    public GameObject winnerText;
    public int inGameMin;

    private float timer;
    private int inGameSeconds;

    // Use this for initialization
    void Start ()
    {
        Player1.GetComponent<PlayerScript>().currentState = PlayerScript.PlayerState.Comander;
        Player2.GetComponent<PlayerScript>().currentState = PlayerScript.PlayerState.Comander;
        theGameState = GameState.PreGame;
        timer = 0.0f;
        inGameMin = 0;
        inGameSeconds = -preGameTime;
    }
	
	// Update is called once per frame
	void Update ()
    {
        // the game time
        if (theGameState != GameState.PostGame)
        {
            timer += Time.deltaTime;

            while (timer >= 1.0f)
            {
                ++inGameSeconds;
                timer -= 1.0f;
            }

            while (inGameSeconds >= 60)
            {
                ++inGameMin;
                inGameSeconds -= 60;
            }

            if (theGameState == GameState.PreGame && inGameSeconds >= 0.0f)
            {
                theGameState = GameState.Playing;
            }
        }
        else
        {
            timer += Time.deltaTime;
            if (timer > 5)
                SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }

        string theClockNumbers = string.Format("{0}{1}:{2}", inGameSeconds < 0.0f ? "-" : "", inGameMin.ToString("#00"), Mathf.Abs(inGameSeconds).ToString("00")); // display the time
        theClock.GetComponent<Text>().text = theClockNumbers;
	}

    public void EndTheGame(Team winner)
    {
        if (theGameState != GameState.PostGame)
        {
            theGameState = GameState.PostGame;
            timer = 0;
            winnerText.GetComponent<Text>().text = "Player " + ((int)winner + 1) + " Wins!"; // declare winner
            winnerText.GetComponent<Text>().enabled = true;
        }
    }

    // find the closest game obeject to a location
    public GameObject GetClosestGameObject(GameObject origin, Team player, float selectorDistance = -1.0f, bool minions = true, bool towers = true, bool champs = true, bool door = true)
    {
        EnemyInfo unit = new EnemyInfo
        {
            targetObj = null,
            targetDist = 0
        };
        // get thje player
        GameObject chosenPlayer;
        switch (player)
        {
            case Team.Team1:
                chosenPlayer = Player1;
                break;
            case Team.Team2:
                chosenPlayer = Player2;
                break;
            default:
                chosenPlayer = null;
                break;
        }

        // check based of the given variables
        if (champs)
            unit = FindClostestOfType(chosenPlayer.GetComponent<ChampManager>().SpawnedChamps, unit, origin, selectorDistance);
        if (towers)
            unit = FindClostestOfType(chosenPlayer.GetComponent<BuildManager>().SpawnedTowers, unit, origin, selectorDistance);
        if (minions)
            unit = FindClostestOfType(GetComponent<MinionSpawner>().GetMinionList(player), unit, origin, selectorDistance);
        if (door)
        {
            GameObject targetDoor;

            switch(player)
            {
                case Team.Team1:
                    targetDoor = GetComponent<MinionSpawner>().TeamDoors[(int)Team.Team1];
                    break;
                case Team.Team2:
                    targetDoor = GetComponent<MinionSpawner>().TeamDoors[(int)Team.Team2];
                    break;
                default:
                    targetDoor = new GameObject();
                    Debug.Log("Error finding door.");
                    break;
            }

            if (unit.targetObj == null && Vector3.Distance(origin.transform.position, targetDoor.transform.position) < unit.targetDist)
                unit.targetObj = targetDoor;
        }

        return unit.targetObj;
    }

    EnemyInfo FindClostestOfType(List<GameObject> searchList, EnemyInfo unitFound, GameObject origin, float selectorDistance)
    {
        foreach (GameObject unit in searchList) //  check list
        {
            float newDist = Vector3.Distance(unit.transform.position, origin.transform.position); // distance
            if (newDist <= selectorDistance || selectorDistance < 0.0f) // is it close enough
            {
                if (unitFound.targetObj == null) // if empty set it
                {
                    unitFound.targetObj = unit;
                    unitFound.targetDist = newDist;
                }
                else
                {
                    if (newDist < unitFound.targetDist) // check if its better
                    {
                        unitFound.targetObj = unit;
                        unitFound.targetDist = newDist;
                    }
                }
            }
        }
        return unitFound;
    }

    public List<GameObject> FindAllClose(GameObject origin, Team teamToCheck, float range = 1.0f, bool minions = true, bool towers = true, bool champs = true, bool door = true)
    {
        List<GameObject> foundList = new List<GameObject>();
        // set the plater
        GameObject chosenPlayer;
        switch (teamToCheck)
        {
            case Team.Team1:
                chosenPlayer = Player1;
                break;
            case Team.Team2:
                chosenPlayer = Player2;
                break;
            default:
                chosenPlayer = null;
                break;
        }
        // get a list of all within range
        // given by the variables
        if (champs)
        {
            foreach (GameObject unit in chosenPlayer.GetComponent<ChampManager>().SpawnedChamps)
            {
                if (unit != null)
                {
                    float newDist = Vector3.Distance(unit.transform.position, origin.transform.position);
                    if (newDist <= range || range < 0.1f)
                    {
                        foundList.Add(unit);
                    }
                }
            }
        }
        
        if (towers)
        {
            foreach (GameObject unit in chosenPlayer.GetComponent<BuildManager>().SpawnedTowers)
            {
                float newDist = Vector3.Distance(unit.transform.position, origin.transform.position);
                if (newDist <= range || range < 0.0f)
                {
                    foundList.Add(unit);
                }
            }
        }
        
        if (minions)
        {
            foreach (GameObject unit in GetComponent<MinionSpawner>().GetMinionList(teamToCheck))
            {
                float newDist = Vector3.Distance(unit.transform.position, origin.transform.position);

                if (newDist <= range || range < 0.0f)
                {
                    foundList.Add(unit);
                }
            }
        }

        if (door)
        {
            GameObject targetDoor;

            switch (teamToCheck)
            {
                case Team.Team1:
                    targetDoor = GetComponent<MinionSpawner>().TeamDoors[(int)Team.Team1];
                    break;
                case Team.Team2:
                    targetDoor = GetComponent<MinionSpawner>().TeamDoors[(int)Team.Team2];
                    break;
                default:
                    targetDoor = new GameObject();
                    Debug.Log("Error finding door.");
                    break;
            }

            if (Vector3.Distance(origin.transform.position, targetDoor.transform.position) < range)
                foundList.Add(targetDoor);
        }

        return foundList;
    }
}
