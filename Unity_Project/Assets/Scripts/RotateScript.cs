﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateScript : MonoBehaviour
{
    public float rotateRate;
    public GameObject dataCoreObj;
    public GameObject sphereObj;

    private float distance;

    private void Start()
    {
        distance = Vector3.Distance(dataCoreObj.transform.position, sphereObj.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        dataCoreObj.transform.RotateAround(transform.position, Vector3.up, rotateRate * Time.deltaTime);
        sphereObj.transform.RotateAround(transform.position, Vector3.up, -2 * rotateRate * Time.deltaTime);
    }
}
