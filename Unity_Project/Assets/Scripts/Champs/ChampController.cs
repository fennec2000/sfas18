﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using System;

public class ChampController : MonoBehaviour, IDamageable, ICombat, IUpgradeable
{
    public enum ChampType { Spinner, PowerFist, Charger, NumOfChampTypes };
    public enum UnitUpgrade { Default = 1, Level2, Level3, NumOfUnitUpgrades };
    public enum ChampMode { Auto, Attack, Hold, NumOfChampModes };
    public enum ChampStyle { Aggressive, Defensive, NumOfChampStyles };
    enum ChampAudioSources { Move, Attack, Hit, NumOfAudioSources };

    public Image Healthbar;
    public GameObject target;
    public Vector3 selectedPos;
    public Vector3 forcedPos;
    public StartingScript.Team myTeam;
    public ChampStyle myStyle;
    public UnitUpgrade myUpgrade;
    public float selectorDistance;
    public float targetUpdateRate;
    public float attackRad;
    public float attackRate;
    public int[] upgradeCost;
    public int[] upgradeMultiplier;
    public int myBaseDamage;
    public int baseMaxHealth;
    public bool forced;
    public GameObject defenceTarget;

    private GameObject closestTarget;
    private GameObject myPlayer;
    private GameObject theGameManager;
    private NavMeshAgent agent;
    private StartingScript.Team myEnemyTeam;
    private ChampType myChampType;
    private ChampMode myMode;
    private float currentTargetUpdateRate;
    private int myHealth;
    private int myHealthMax;
    private int myDamage;
    private float currentAttackTime;
    private int myCost;
    private AudioSource[] source;
    private AudioClip[] champSounds;
    private Vector3 lastPos;

    // Use this for initialization
    void Awake()
    { // setup
        myHealth = baseMaxHealth;
        myHealthMax = baseMaxHealth;
        myDamage = myBaseDamage;
        agent = GetComponent<NavMeshAgent>();
        myMode = ChampMode.Auto;
        myStyle = ChampStyle.Defensive;
        currentTargetUpdateRate = 0;
        forced = false;
        currentAttackTime = 0;
        theGameManager = GameObject.Find("GameManager");

        switch (myTeam)
        {
            case StartingScript.Team.Team1:
                myEnemyTeam = StartingScript.Team.Team2;
                myPlayer = theGameManager.GetComponent<StartingScript>().Player1;
                break;
            case StartingScript.Team.Team2:
                myEnemyTeam = StartingScript.Team.Team1;
                myPlayer = theGameManager.GetComponent<StartingScript>().Player2;
                break;
        }

        myCost = myPlayer.GetComponent<ChampManager>().champCosts[(int)myChampType];
    }

    private void Start()
    {
        // setup
        lastPos = transform.position;
        champSounds = new AudioClip[(int)ChampAudioSources.NumOfAudioSources];
        champSounds[(int)ChampAudioSources.Move] = Resources.Load("Audio/TheLibrarybyMTC_Robo_Motor_CoffeeGrinderB_001") as AudioClip;
        champSounds[(int)ChampAudioSources.Attack] = Resources.Load("Audio/TheLibrarybyMTC_Robo_LaserBlast_Medium_027") as AudioClip;
        champSounds[(int)ChampAudioSources.Hit] = Resources.Load("Audio/Plate_Impact_Hard_02") as AudioClip;
        source = new AudioSource[(int)ChampAudioSources.NumOfAudioSources];
        for (int i = 0; i < (int)ChampAudioSources.NumOfAudioSources; ++i)
        {
            source[i] = gameObject.AddComponent<AudioSource>();
            source[i].playOnAwake = false;
            source[i].Stop();
            source[i].clip = champSounds[i];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (lastPos != transform.position) // sound if moved
        {
            if (source[(int)ChampAudioSources.Move].isPlaying)
                source[(int)ChampAudioSources.Move].Play();
            lastPos = transform.position;
        }
        else
            source[(int)ChampAudioSources.Move].Stop();

        if (currentTargetUpdateRate > 0)
            currentTargetUpdateRate -= Time.deltaTime;
        if (currentAttackTime > 0)
            currentAttackTime -= Time.deltaTime;

        if (closestTarget == null || currentTargetUpdateRate <= 0) // check targets if none or too long on one target
        {
            closestTarget = theGameManager.GetComponent<StartingScript>().GetClosestGameObject(gameObject, myEnemyTeam, selectorDistance, false);
            currentTargetUpdateRate = targetUpdateRate;
        }

        switch (myMode)
        {
            case ChampMode.Auto:
                switch (myStyle)
                {
                    case ChampStyle.Aggressive: // push up lane with minions
                        // find tower closest to door and attack
                        if (target == null)
                            target = theGameManager.GetComponent<StartingScript>().GetClosestGameObject(theGameManager.GetComponent<MinionSpawner>().TeamDoors[(int)myTeam], myEnemyTeam, -1, true, true, true, false);
                        
                        if (target != null)
                        {
                            if (target != null && Vector3.Distance(target.transform.position, transform.position) < attackRad)
                                Attack();
                            agent.SetDestination(target.transform.position);
                            agent.Resume();
                        }

                        break;
                    case ChampStyle.Defensive: // find closest tower to enemy door
                        // find tower closest to emeny door
                        // else go to door
                        if (defenceTarget == null)
                            RecalcDefenciveTarget();

                        // defend
                        target = theGameManager.GetComponent<StartingScript>().GetClosestGameObject(gameObject, myEnemyTeam, selectorDistance, true, false, true, false);
                        
                        if (target != null)
                        {
                            agent.SetDestination(target.transform.position);
                            Attack();
                        }
                        else if (defenceTarget != null && Vector3.Distance(defenceTarget.transform.position, transform.position) > selectorDistance / 2)
                        {
                            agent.SetDestination(defenceTarget.transform.position);
                            agent.Resume();
                        }
                        else
                        {
                            source[(int)ChampAudioSources.Move].Stop();
                            agent.isStopped = true;
                        }
                        break;
                }
                break;
            case ChampMode.Attack: // attack on the way to point
                // if enemy is close attack
                closestTarget = theGameManager.GetComponent<StartingScript>().GetClosestGameObject(gameObject, myEnemyTeam, attackRad);
                if (closestTarget != null && Vector3.Distance(closestTarget.transform.position, transform.position) < attackRad)
                {
                    Attack();
                }
                else if (forced) // else move
                {
                    float dist = Vector3.Distance(forcedPos, transform.position);
                    // move to target
                    if (dist > attackRad && agent.destination != forcedPos)
                    {
                        agent.SetDestination(forcedPos);
                        agent.Resume();
                    }
                    // go back to auto once point is hit
                    else if (dist < attackRad)
                    {
                        ChangeMode(ChampMode.Auto);
                    }
                }
                else
                {
                    float dist = Vector3.Distance(target.transform.position, transform.position);
                    if (dist > attackRad && agent.destination != target.transform.position)
                    {
                        agent.SetDestination(target.transform.position);
                        agent.Resume();
                    }
                    else if (dist < attackRad)
                        ChangeMode(ChampMode.Auto);
                }
                break;
            case ChampMode.Hold: // attack only near
                // move to target
                if (agent.destination != forcedPos && Vector3.Distance(forcedPos, transform.position) > attackRad)
                {
                    agent.SetDestination(forcedPos);
                    agent.Resume();
                }
                // defend
                closestTarget = theGameManager.GetComponent<StartingScript>().GetClosestGameObject(gameObject, myEnemyTeam, attackRad);
                if (closestTarget != null && Vector3.Distance(closestTarget.transform.position, transform.position) < attackRad)
                    Attack();
                break;
        }
    }

    // get the champs data
    public void ChampSetup(ChampType givenType, UnitUpgrade givenUpgrade = UnitUpgrade.Default)
    {
        myChampType = givenType;
        myUpgrade = givenUpgrade;

        switch (myChampType)
        {
            case ChampType.Spinner:
                gameObject.AddComponent<ChampAttSpinner>();
                GetComponent<ChampAttSpinner>().Setup(myEnemyTeam);
                break;
            case ChampType.PowerFist:
                gameObject.AddComponent<ChampAttPowerFist>();
                break;
            case ChampType.Charger:
                gameObject.AddComponent<ChampAttCharger>();
                break;
        }
    }

    public ChampType GetChampType()
    {
        return myChampType;
    }

    public void TakeDamage(int damage)
    {
        source[(int)ChampAudioSources.Hit].Stop();
        source[(int)ChampAudioSources.Hit].Play();
        myHealth -= damage;

        if (!this.gameObject.GetComponentInChildren<Canvas>().enabled)
            this.gameObject.GetComponentInChildren<Canvas>().enabled = true;

        Healthbar.fillAmount = myHealth / (float)myHealthMax;

        if (0 >= myHealth) // deathcheck
            transform.parent.GetComponent<ChampManager>().KillChamp(gameObject);
    }

    public void Attack()
    {
        if (currentAttackTime <= 0)
        {
            source[(int)ChampAudioSources.Attack].Stop();
            source[(int)ChampAudioSources.Attack].Play();
            currentAttackTime = attackRate;
            switch (myChampType)
            {
                case ChampType.Spinner:
                    gameObject.GetComponent<ChampAttSpinner>().SpinnerAtt(myDamage, attackRad);
                    break;
                case ChampType.PowerFist:
                    gameObject.GetComponent<ChampAttPowerFist>().PowerFistAtt(myDamage, target);
                    break;
                case ChampType.Charger:
                    gameObject.GetComponent<ChampAttCharger>().ChargerAtt(myDamage, target);
                    break;
            }
        }
    }

    public void ChangeMode(ChampMode newChampMode)
    {
        myMode = newChampMode;
        forced = false;
        Debug.Log("Changeing mode: " + newChampMode.ToString());
    }

    public void ForceMode(ChampMode newChampMode, Vector3 newPos)
    {
        myMode = newChampMode;
        forcedPos = newPos;
        forced = true;
        Debug.Log("Being forced.");
    }

    public void AttackUnit(GameObject newTarget)
    {
        target = newTarget;
        ChangeMode(ChampMode.Attack);
    }

    public bool AffordUpgrade()
    {
        if (myUpgrade < UnitUpgrade.NumOfUnitUpgrades - 1)
            return myPlayer.GetComponent<PlayerScript>().money >= ((int)myUpgrade + 1) * myCost && myUpgrade < UnitUpgrade.NumOfUnitUpgrades;
        return false;
    }

    public int UpgradeCost()
    {
        if (myUpgrade < UnitUpgrade.NumOfUnitUpgrades - 1)
            return ((int)myUpgrade + 1) * myCost;
        return Int32.MaxValue; // return max if no upgrade avalable
    }

    public void UpgradeUnit()
    {
        if (AffordUpgrade())
        {
            myPlayer.GetComponent<PlayerScript>().UpdateMoney(-((int)myUpgrade + 1) * myCost);
            ++myUpgrade;
            myHealthMax = baseMaxHealth * (int)myUpgrade;
            myHealth = myHealthMax;
            myDamage = myBaseDamage * (int)myUpgrade;
        }
        else
            Debug.Log("Upgrade failed: " + gameObject.name);
    }

    public void RecalcDefenciveTarget()
    {
        defenceTarget = theGameManager.GetComponent<StartingScript>().GetClosestGameObject(theGameManager.GetComponent<MinionSpawner>().TeamDoors[(int)myEnemyTeam], myTeam, -1, false, true, false, true);
    }
}
