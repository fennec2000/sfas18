﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ChampAttSpinner : MonoBehaviour
{
    private GameObject theGameManager;
    private StartingScript.Team myEnemy;

    public void Awake()
    {
        theGameManager = GameObject.Find("GameManager");
    }

    public void Setup(StartingScript.Team theEnemy)
    {
        myEnemy = theEnemy;
    }
    
    public void SpinnerAtt(int damage, float range)
    {
        foreach (GameObject unit in theGameManager.GetComponent<StartingScript>().FindAllClose(gameObject, myEnemy, range))
        {

            if (unit.GetComponent<ChampController>() != null) // champ
            {
                unit.GetComponent<ChampController>().TakeDamage(damage);
            }
            else if (unit.GetComponent<TowerController>() != null) // tower
            {
                unit.GetComponent<TowerController>().TakeDamage(damage);
            }
            else if (unit.GetComponent<MinionController>() != null) // minion
            {
                unit.GetComponent<MinionController>().TakeDamage(damage);
            }
            else if (unit.GetComponent<DoorScript>() != null && unit.GetComponent<DoorScript>().myTeam == myEnemy) // enemy door
            {
                unit.GetComponent<DoorScript>().TakeDamage(damage);
            }
        }
    }
}

public class ChampAttPowerFist : MonoBehaviour
{
    public void PowerFistAtt(int damage, GameObject enemy)
    {
        if (enemy.GetComponent<ChampController>() != null) // champ
        {
            enemy.GetComponent<ChampController>().TakeDamage(damage);
        }
        else if (enemy.GetComponent<TowerController>() != null) // tower
        {
            enemy.GetComponent<TowerController>().TakeDamage(damage);
        }
        else if (enemy.GetComponent<MinionController>() != null) // minion
        {
            enemy.GetComponent<MinionController>().TakeDamage(damage);
        }
        else if (enemy.GetComponent<DoorScript>() != null) // door
        {
            enemy.GetComponent<DoorScript>().TakeDamage(damage);
        }
    }
}

class ChargedUnit
{
    public GameObject enemy;
    public float debuffedTime;
}

public class ChampAttCharger : MonoBehaviour
{
    private List<ChargedUnit> Hit;
    private int FirstHitBuff;
    private float ChargedDebuff;

    private void Awake()
    {
        Hit = new List<ChargedUnit>();
    }

    private void Update()
    {
        if (Hit.Count > 0)
        {
            for(int i = 0; i < Hit.Count; ++i)
            {
                Hit[i].debuffedTime -= Time.deltaTime;
            }

            Hit.Remove(Hit.Single(s => s.debuffedTime <= 0));
        }
    }

    public void ChargerAtt(int damage, GameObject enemy)
    {
        if (!Hit.Any(s => s.enemy == enemy))
        {
            ChargedUnit newUnit = new ChargedUnit
            {
                enemy = enemy,
                debuffedTime = ChargedDebuff
            };
            Hit.Add(newUnit);
            damage += FirstHitBuff;
        }

        if (enemy.GetComponent<ChampController>() != null) // champ
        {
            enemy.GetComponent<ChampController>().TakeDamage(damage);
        }
        else if (enemy.GetComponent<TowerController>() != null) // tower
        {
            enemy.GetComponent<TowerController>().TakeDamage(damage);
        }
        else if (enemy.GetComponent<MinionController>() != null) // minion
        {
            enemy.GetComponent<MinionController>().TakeDamage(damage);
        }
        else if (enemy.GetComponent<DoorScript>() != null) // door
        {
            enemy.GetComponent<DoorScript>().TakeDamage(damage);
        }
    }
}
