﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioListenerScript : MonoBehaviour
{
    public GameObject[] Cameras;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 listenerPos = new Vector3(0, 0, 0);

        foreach(GameObject cam in Cameras)
            listenerPos += cam.transform.position;
        transform.position = listenerPos / Cameras.Length;
    }
}
